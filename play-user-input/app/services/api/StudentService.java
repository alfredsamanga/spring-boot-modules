package services.api;

import com.google.inject.ImplementedBy;
import model.Student;
import services.impl.StudentServiceImpl;

import java.util.List;
import java.util.Optional;

@ImplementedBy(StudentServiceImpl.class)
public interface StudentService {
    Optional<Student> create(Student student);
    Optional<Student> getById(Long studentId);
    List<Student> getAll();
    Optional<Student> update(Student student);
    void delete(Long studentId);
}
