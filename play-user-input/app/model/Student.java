package model;

import play.data.validation.Constraints;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

public class Student {
    private Long id;

    @Constraints.Required(message = "First name is required")
    private String firstName;

    @Constraints.Required(message = "Last name is required")
    private String lastName;

    @Constraints.Required(message = "Email is required")
    @Constraints.Email(message = "Provide a valid email")
    private String email;

    @Max(value = 10, message = "Provide a level less than 10")
    @Min(value = 1, message = "Provide a level greater than 0")
    private Integer level;

    public Student() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", level=" + level +
                '}';
    }
}
