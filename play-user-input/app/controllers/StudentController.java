package controllers;

import lombok.extern.slf4j.Slf4j;
import model.Student;
import play.data.DynamicForm;
import play.data.Form;
import play.data.FormFactory;
import play.i18n.MessagesApi;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import services.api.StudentService;

import javax.inject.Inject;

@Slf4j
public class StudentController extends Controller {
    private final FormFactory formFactory;
    private final MessagesApi messagesApi;
    private final StudentService studentService;

    @Inject
    public StudentController(FormFactory formFactory, MessagesApi messagesApi, StudentService studentService) {
        this.formFactory = formFactory;
        this.studentService = studentService;
        this.messagesApi = messagesApi;
    }

    public Result createStudentPage(Http.Request request) {
        Form<Student> studentForm = formFactory.form(Student.class);
        return ok(views.html.students.create_student.render(studentForm, messagesApi.preferred(request)));
    }

    public Result createStudent(Http.Request request) {
        Form<Student> studentForm = formFactory.form(Student.class).bindFromRequest(request);
        if (studentForm.hasErrors()) {
            return badRequest(views.html.students.create_student.render(studentForm, messagesApi.preferred(request)));
        } else {
            Student student = studentForm.get();
            return studentService.create(student)
                    .map(createdStudent -> redirect(routes.StudentController.viewStudent(createdStudent.getId())))
                    .orElse(redirect(routes.StudentController.createStudentPage()));
        }
    }

    public Result createStudentWithDynamicForms(Http.Request request) {
        DynamicForm dynamicForm = formFactory.form().bindFromRequest(request);
        String firstName = dynamicForm.get("firstName");
        String lastName = dynamicForm.get("lastName");
        String email = dynamicForm.get("email");
        int level = Integer.parseInt(dynamicForm.get("level"));

        Student student = new Student();
        student.setFirstName(firstName);
        student.setLastName(lastName);
        student.setEmail(email);
        student.setLevel(level);

        return studentService.create(student)
                .map(createdStudent -> redirect(routes.StudentController.viewStudent(createdStudent.getId())))
                .orElse(redirect(routes.StudentController.createStudentPage()));
    }

    public Result viewStudent(Http.Request request, Long studentId) {
        return studentService.getById(studentId)
                .map(student -> ok(views.html.students.view_student.render(student)))
                .orElse(redirect(routes.StudentController.viewStudents()));
    }

    public Result viewStudents(Http.Request request) {
        return ok(views.html.students.view_students.render(studentService.getAll()));
    }

    public Result updateStudentPage(Http.Request request, Long studentId) {
        return studentService.getById(studentId).map(student -> {
            Form<Student> studentForm = formFactory.form(Student.class).fill(student);
            return ok(views.html.students.update_student.render(studentForm, messagesApi.preferred(request)));
        }).orElse(redirect(routes.StudentController.viewStudents()));
    }

    public Result updateStudent(Http.Request request) {
        Form<Student> studentForm = formFactory.form(Student.class).bindFromRequest(request);
        if (studentForm.hasErrors()) {
            return badRequest(views.html.students.update_student.render(studentForm, messagesApi.preferred(request)));
        } else {
            Student student = studentForm.get();
            return studentService.update(student)
                    .map(updatedStudent -> redirect(routes.StudentController.viewStudent(updatedStudent.getId())))
                    .orElse(redirect(routes.StudentController.viewStudents()));
        }
    }

    public Result deleteStudent(Http.Request request, Long studentId) {
        studentService.delete(studentId);
        return redirect(routes.StudentController.viewStudents());
    }
}
