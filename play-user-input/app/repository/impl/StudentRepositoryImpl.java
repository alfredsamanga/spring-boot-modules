package repository.impl;

import model.Student;
import repository.api.StudentRepository;

import javax.inject.Singleton;
import java.util.*;

@Singleton
public class StudentRepositoryImpl implements StudentRepository {
    private Map<Long, Student> studentMap = new HashMap<>();

    @Override
    public Optional<Student> createStudent(Student student) {
        final long studentId = studentMap.size() + 1;
        student.setId(studentId);
        studentMap.put(studentId, student);
        return Optional.of(student);
    }

    @Override
    public Optional<Student> updateStudent(Student student) {
        final Student updatedStudent = studentMap.computeIfPresent(student.getId(), (studentId, currentStudent) -> student);
        return Optional.ofNullable(updatedStudent);
    }

    @Override
    public Optional<Student> getStudentById(Long studentId) {
        return Optional.ofNullable(studentMap.get(studentId));
    }

    @Override
    public Optional<Student> deleteStudentById(Long studentId) {
        final Student removedStudent = studentMap.remove(studentId);
        return Optional.ofNullable(removedStudent);
    }

    @Override
    public List<Student> getAll() {
        return new ArrayList<>(studentMap.values());
    }
}
