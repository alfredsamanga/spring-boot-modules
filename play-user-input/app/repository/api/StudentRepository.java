package repository.api;

import com.google.inject.ImplementedBy;
import model.Student;
import repository.impl.StudentRepositoryImpl;

import java.util.List;
import java.util.Optional;

@ImplementedBy(StudentRepositoryImpl.class)
public interface StudentRepository {
    Optional<Student> createStudent(Student student);
    Optional<Student> updateStudent(Student student);
    Optional<Student> getStudentById(Long studentId);
    Optional<Student> deleteStudentById(Long studentId);
    List<Student> getAll();
}
