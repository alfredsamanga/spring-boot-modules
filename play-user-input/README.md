# ResiliaTech Modules
This repository contains code for the articles on [resiliatech.com](https://resiliatech.com/).

This module is linked to the following article: 
[How To Handle User Input With Play Framework](https://resiliatech.com/user-input-play-framework)
