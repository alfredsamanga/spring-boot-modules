package com.resiliatech.restapi;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@WebMvcTest(CarController.class)
public class CarControllerTests {
    @Autowired
    private MockMvc mockMvc;

    private Car car;

    @BeforeEach
    public void init() {
        car = getCar();
    }

    @AfterEach
    public void destroy() throws Exception {
        deleteData();
        car = null;
    }

    @Test
    public void givenCar_whenCreate_thenShouldReturnCarAndCorrectStatus() throws Exception {
        mockMvc.perform(post("/cars")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(getCarJson(car)))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name", is(car.getName())));
    }

    @Test
    public void givenExistingCar_whenGet_thenShouldReturnCar() throws Exception {
        addData();
        mockMvc.perform(get("/cars/" + car.getId()))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is(car.getName())));
    }

    @Test
    public void givenNonExistingCar_whenGet_thenShouldReturnNotFoundStatus() throws Exception {
        mockMvc.perform(get("/cars/" + car.getId()))
                .andExpect(status().isNotFound());
    }

    @Test
    public void givenExistingCars_whenGet_thenShouldReturnCarCollection() throws Exception {
        addData();
        mockMvc.perform(get("/cars"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].name", is(car.getName())));
    }

    @Test
    public void givenNonExistentCars_whenGet_thenShouldReturnEmptyCarCollection() throws Exception {
        mockMvc.perform(get("/cars"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()", is(0)));
    }

    @Test
    public void givenCar_whenUpdate_thenShouldReturnUpdateCarAndCorrectStatus() throws Exception {
        addData();
        Car updatedCar = getUpdatedCar();
        mockMvc.perform(put("/cars")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(getCarJson(updatedCar)))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isAccepted())
                .andExpect(jsonPath("$.name", is(updatedCar.getName())));
    }

    @Test
    public void givenNonExistentCar_whenUpdate_thenShouldReturn404NotFoundStatus() throws Exception {
        Car updatedCar = getUpdatedCar();
        mockMvc.perform(put("/cars")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(getCarJson(updatedCar)))
                .andExpect(status().isNotFound());
    }

    @Test
    public void givenExistingCar_whenDelete_thenShouldReturnDeletedCarAndCorrectStatus() throws Exception {
        addData();
        mockMvc.perform(delete("/cars/" + car.getId()))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isAccepted())
                .andExpect(jsonPath("$.name", is(car.getName())));
    }

    @Test
    public void givenNonExistentCar_whenDelete_thenShouldReturnCorrectStatus() throws Exception {
        mockMvc.perform(delete("/cars/" + car.getId()))
                .andExpect(status().isNotFound());
    }

    private void addData() throws Exception {
        mockMvc.perform(post("/cars")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(getCarJson(car)));
    }

    private void deleteData() throws Exception {
        mockMvc.perform(delete("/cars/" + car.getId()));
    }

    private String getCarJson(Car c) {
        return String.format("{" +
                "\"id\":%d, " +
                "\"name\":\"%s\"," +
                "\"make\":\"%s\"," +
                "\"model\":\"%s\"," +
                "\"year\":%d}", c.getId(), c.getName(), c.getMake(), c.getModel(), c.getYear());
    }

    private Car getCar() {
        Car car = new Car();
        car.setId(1L);
        car.setName("Mazda Car");
        car.setMake("Mazda");
        car.setModel("Axela");
        car.setYear(2005);
        return car;
    }

    private Car getUpdatedCar() {
        Car car = new Car();
        car.setId(1L);
        car.setName("My Toyota");
        car.setMake("Toyota");
        car.setModel("Hilux");
        car.setYear(2020);
        return car;
    }
}
