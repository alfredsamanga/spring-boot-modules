package com.resiliatech.carservice.controller;

import com.resiliatech.carservice.service.CarService;
import com.resiliatech.sharedlib.dto.CarDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/cars")
public class CarController {
    private final CarService carService;

    @GetMapping
    public ResponseEntity<List<CarDTO>> listCars() {
        return new ResponseEntity<>(carService.list(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<CarDTO> getCarById(@PathVariable("id") Long id) {
        return carService.getById(id).map(carDTO -> new ResponseEntity<>(carDTO, HttpStatus.OK))
                .orElse(ResponseEntity.notFound().build());
    }

    @GetMapping("/owner/{ownerId}")
    public ResponseEntity<List<CarDTO>> getCarsByOwnerId(@PathVariable("ownerId") Long ownerId) {
        return new ResponseEntity<>(carService.getCarsByOwner(ownerId), HttpStatus.OK);
    }
}
