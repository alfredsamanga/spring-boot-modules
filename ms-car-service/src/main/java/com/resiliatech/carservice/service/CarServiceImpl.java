package com.resiliatech.carservice.service;
import java.time.LocalDate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.resiliatech.sharedlib.dto.CarDTO;
import com.resiliatech.sharedlib.dto.PersonDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class CarServiceImpl implements CarService {
@LoadBalanced
private final RestTemplate restTemplate;
    private final ResourceLoader resourceLoader;
    private final ObjectMapper objectMapper;


    private static Map<Long, CarDTO> carsMap;

    @PostConstruct
    public void init() throws IOException {
        final Resource resource = resourceLoader.getResource("classpath:cars.json");
        final List<CarDTO> carDTOS = objectMapper.readValue(resource.getInputStream(), new TypeReference<List<CarDTO>>() {});
        carsMap = carDTOS.stream().collect(Collectors.toMap(CarDTO::getId, Function.identity()));
    }

    @Override
    public List<CarDTO> list() {
        return new ArrayList<>(carsMap.values());
    }

    @Override
    public Optional<CarDTO> getById(Long id) {
        final CarDTO car = carsMap.get(id);
        if(car == null) return Optional.empty();
        log.info("Car is: {}", car);
        final ResponseEntity<PersonDTO> responseEntity = restTemplate.exchange("http://person-service/people/" + car.getOwnerId(),
                HttpMethod.GET,
                HttpEntity.EMPTY,
                new ParameterizedTypeReference<PersonDTO>() {
                });
        if(responseEntity.getStatusCode() == HttpStatus.OK) {
            final PersonDTO owner = responseEntity.getBody();
            car.setOwner(owner);
        }
        return Optional.ofNullable(car);
    }

    @Override
    @HystrixCommand(fallbackMethod="getDefaultCars")
    public List<CarDTO> getCarsByOwner(Long ownerId) {
        return carsMap.values().stream().filter(c -> c.getOwnerId() == ownerId).collect(Collectors.toList());
    }

    public List<CarDTO> getDefaultCars(Long ownerId) {
        final CarDTO carDTO = new CarDTO();
        carDTO.setId(100L);
        carDTO.setMake("Mazda");
        carDTO.setModel("Defalut");
        carDTO.setDateBought(LocalDate.now());
        carDTO.setOwnerId(ownerId);

        return Arrays.asList(carDTO);
    }
}
