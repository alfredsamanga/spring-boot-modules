package com.resiliatech.carservice.service;

import com.resiliatech.sharedlib.dto.CarDTO;

import java.util.List;
import java.util.Optional;

public interface CarService {
    List<CarDTO> list();
    Optional<CarDTO> getById(Long id);
    List<CarDTO> getCarsByOwner(Long ownerId);
}
