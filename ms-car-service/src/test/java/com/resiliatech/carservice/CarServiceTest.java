package com.resiliatech.carservice;

import com.resiliatech.carservice.service.CarService;
import com.resiliatech.carservice.service.CarServiceImpl;
import com.resiliatech.sharedlib.dto.CarDTO;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class CarServiceTest {
    @Autowired
    private CarService carService;

    @Test
    @DisplayName("Checks if initialisation completed successfully")
    public void givenCarsService_whenListCars_shouldReturnNonEmptyList() throws Exception {
        final List<CarDTO> list = carService.list();
        assertAll(
                () -> assertThat(list).isNotNull(),
                () -> assertThat(list).isNotEmpty(),
                () -> assertEquals(list.size(), 22, "There should be 22 cars")
        );
    }

    @Test
    public void givenCarOwnerId_whenListCars_shouldReturnNonEmptyList() throws Exception {
        final List<CarDTO> list = carService.getCarsByOwner(1L);
        assertAll(
                () -> assertThat(list).isNotNull(),
                () -> assertThat(list).isNotEmpty(),
                () -> assertEquals(1L, list.get(0).getOwnerId())
        );
    }

    @Test
    public void givenCarId_whenGetCar_shouldReturnCar() throws Exception {
        final Optional<CarDTO> carDTOOptional = carService.getById(1L);
        assertAll(
                () -> assertThat(carDTOOptional).isNotNull(),
                () -> assertThat(carDTOOptional).isPresent(),
                () -> assertEquals(1L, carDTOOptional.get().getId())
        );
    }
}
