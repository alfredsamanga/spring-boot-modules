# ResiliaTech Modules
This repository contains code for the articles on [resiliatech.com](https://resiliatech.com/).

This module is linked to the following article:
[Microservice Applications With Spring Boot](https://resiliatech.com/spring-boot-microservices/)

