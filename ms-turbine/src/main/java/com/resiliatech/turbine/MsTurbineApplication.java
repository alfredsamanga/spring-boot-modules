package com.resiliatech.turbine;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.turbine.EnableTurbine;

@EnableTurbine
@SpringBootApplication
public class MsTurbineApplication {
	public static void main(String[] args) {
		SpringApplication.run(MsTurbineApplication.class, args);
	}
}
