package com.resiliatech.sharedlib.dto;

import lombok.Data;

import java.util.List;

@Data
public class PersonDTO {
    private Long id;
    private String firstName;
    private String lastName;
    private List<CarDTO> cars;
}
