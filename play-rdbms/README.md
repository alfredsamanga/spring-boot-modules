# ResiliaTech Modules
This repository contains code for the articles on [resiliatech.com](https://resiliatech.com/).

This module is linked to the following article: 
[Accessing Relational Databases With Play Framework](https://resiliatech.com/rdbms-play-framework)
