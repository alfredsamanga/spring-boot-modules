name := """play-rdbms"""
organization := "com.resiliatech"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.13.3"

libraryDependencies ++= Seq(
  guice,
  javaJpa,
  "org.projectlombok" % "lombok" % "1.18.16" % "provided",
  "org.postgresql" % "postgresql" % "42.2.18",
  "mysql" % "mysql-connector-java" % "8.0.22",
  "org.hibernate" % "hibernate-core" % "5.4.9.Final" // replace by your jpa implementation
)
