package repository.impl;

import ec.DatabaseExecutionContext;
import model.Student;
import org.hibernate.Session;
import org.hibernate.query.Query;
import play.db.jpa.JPAApi;
import repository.api.StudentRepository;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.CompletionStage;
import java.util.function.Function;

import static java.util.concurrent.CompletableFuture.supplyAsync;

@Singleton
public class StudentRepositoryImpl implements StudentRepository {
    private JPAApi jpaApi;
    private DatabaseExecutionContext executionContext;

    @Inject
    public StudentRepositoryImpl(JPAApi jpaApi, DatabaseExecutionContext executionContext) {
        this.jpaApi = jpaApi;
        this.executionContext = executionContext;
    }

    private <T> T wrap(Function<EntityManager, T> function) {
        return jpaApi.withTransaction(function);
    }

    @Override
    public CompletionStage<Optional<Student>> createStudent(Student student) {
        return supplyAsync(() -> wrap(entityManager -> {
            student.setId(UUID.randomUUID().toString());
            entityManager.persist(student);
            return Optional.ofNullable(student);
        }), executionContext);
    }

    @Override
    public CompletionStage<Optional<Student>> updateStudent(Student student) {
        return supplyAsync(() -> wrap(entityManager -> {
            entityManager.merge(student);
            return Optional.ofNullable(student);
        }), executionContext);
    }

    @Override
    public CompletionStage<Optional<Student>> getStudentById(String studentId) {
        return supplyAsync(() -> wrap(entityManager ->
                Optional.ofNullable(entityManager.find(Student.class, studentId))), executionContext);
    }

    @Override
    public CompletionStage<Optional<Student>> deleteStudentById(String studentId) {
        return supplyAsync(() -> wrap(entityManager -> {
            final Student student = entityManager.find(Student.class, studentId);
            if(student != null) entityManager.remove(student);
            return Optional.ofNullable(student);
        }), executionContext);
    }

    @Override
    public CompletionStage<List<Student>> getAll() {
        return supplyAsync(() -> wrap(entityManager -> {
            final Session session = entityManager.unwrap(Session.class);
            final TypedQuery<Student> query = session.createNamedQuery("Student.getAll", Student.class);
            return query.getResultList();
        }), executionContext);
    }
}
