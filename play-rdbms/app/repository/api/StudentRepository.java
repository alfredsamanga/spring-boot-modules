package repository.api;

import com.google.inject.ImplementedBy;
import model.Student;
import repository.impl.StudentRepositoryImpl;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletionStage;

@ImplementedBy(StudentRepositoryImpl.class)
public interface StudentRepository {
    CompletionStage<Optional<Student>> createStudent(Student student);
    CompletionStage<Optional<Student>> updateStudent(Student student);
    CompletionStage<Optional<Student>> getStudentById(String studentId);
    CompletionStage<Optional<Student>> deleteStudentById(String studentId);
    CompletionStage<List<Student>> getAll();
}
