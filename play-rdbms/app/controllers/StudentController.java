package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import model.Student;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import services.api.StudentService;
import utils.Util;

import javax.inject.Inject;
import java.util.concurrent.CompletionStage;

public class StudentController extends Controller {
    private StudentService studentService;
    private HttpExecutionContext executionContext;

    @Inject
    public StudentController(StudentService studentService, HttpExecutionContext executionContext) {
        this.studentService = studentService;
        this.executionContext = executionContext;
    }

    public CompletionStage<Result> createStudent(Http.Request request) {
        final JsonNode jsonNode = request.body().asJson();
        final Student student = Util.mapToObject(jsonNode, Student.class);
        return studentService.create(student).thenApplyAsync(studentOptional -> studentOptional.map(s -> {
            return ok(Util.createResponse(s, true));
        }).orElse(notFound(Util.createResponse("Student not found", false))));
    }

    public CompletionStage<Result> updateStudent(Http.Request request) {
        final JsonNode jsonNode = request.body().asJson();
        final Student student = Util.mapToObject(jsonNode, Student.class);
        return studentService.update(student).thenApplyAsync(studentOptional -> studentOptional.map(s -> {
            return ok(Util.createResponse(s, true));
        }).orElse(notFound(Util.createResponse("Student not found", false))));
    }

    public CompletionStage<Result> getStudentById(Http.Request request, String studentId) {
        return studentService.getById(studentId).thenApplyAsync(studentOptional -> studentOptional.map(student -> {
            return ok(Util.createResponse(student, true));
        }).orElse(notFound(Util.createResponse("Student not found", false))));
    }

    public CompletionStage<Result> deleteStudentById(Http.Request request, String studentId) {
        return studentService.delete(studentId).thenApplyAsync(studentOptional -> studentOptional.map(student -> {
            return ok(Util.createResponse(student, true));
        }).orElse(notFound(Util.createResponse("Student not found", false))));
    }

    public CompletionStage<Result> getStudents(Http.Request request) {
        return studentService.getAll().thenApplyAsync(students -> {
            return ok(Util.createResponse(students, true));
        });
    }

}
