package services.api;

import com.google.inject.ImplementedBy;
import model.Student;
import services.impl.StudentServiceImpl;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletionStage;

@ImplementedBy(StudentServiceImpl.class)
public interface StudentService {
    CompletionStage<Optional<Student>> create(Student student);
    CompletionStage<Optional<Student>> getById(String studentId);
    CompletionStage<List<Student>> getAll();
    CompletionStage<Optional<Student>> update(Student student);
    CompletionStage<Optional<Student>> delete(String studentId);
}
