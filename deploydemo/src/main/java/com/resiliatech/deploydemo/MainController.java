package com.resiliatech.deploydemo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class MainController {
    @GetMapping
    public List<String> listNames() {
        return List.of("Alfred", "Anotida", "Jabulani", "Exalt", "Cosmas");
    }
}
