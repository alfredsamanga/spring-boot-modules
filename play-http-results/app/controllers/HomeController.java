package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import play.libs.Json;
import play.mvc.*;
import play.twirl.api.Html;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

public class HomeController extends Controller {

    public Result index(Http.Request request) {
        System.out.println(request.flash().get("message").orElse("No message was set"));
        return ok(views.html.index.render(request));
    }

    public Result download(Http.Request request) {
        Integer downloadCount = request.session().get("downloadCount").map(Integer::parseInt).orElse(0);
        return ok("Download started").addingToSession(request, "downloadCount", String.valueOf(++downloadCount));
    }

    public Result clearDownloadCount(Http.Request request) {
        return ok("Downloads reset").removingFromSession(request, "downloadCount");
    }

    public Result flash(Http.Request request) {
        return redirect("/").flashing("message", "Your request was successfully processed.");
    }

    public Result clearSession(Http.Request request) {
        return ok("Session Cleared").withNewSession();
    }

    public Result indexWithCookies(Http.Request request) {
        return ok(views.html.index.render(request))
                .withCookies(Http.Cookie.builder("lastVisitWasAt", getLastVisitTime())
                        .withMaxAge(Duration.ofMinutes(30))
                        .withDomain(".resiliatech.com")
                        .withPath("/http-results-play-framework")
                        .withSameSite(Http.Cookie.SameSite.STRICT)
                        .withSecure(true)
                        .withHttpOnly(true)
                        .build(), Http.Cookie.builder("another", "one").build());
    }

    public Result indexWithMultipleCookies(Http.Request request) {
        return ok(views.html.index.render(request)).withCookies(
                Http.Cookie.builder("one", "1").build(),
                Http.Cookie.builder("two", "2").build(),
                Http.Cookie.builder("three", "3").build()
        );
    }

    public Result indexDiscardingCookies(Http.Request request) {
        return ok(views.html.index.render(request)).discardingCookie("two");
    }

    private String getLastVisitTime() {
        return OffsetDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME);
    }

    public Result setContentTypeHtmlExplicit() {
        return ok("<p>This is a p tag with <b>some bold</b> text").as("text/html");
    }

    public Result setContentTypeHtml() {
        Html html = Html.apply("<p>This is a p tag with <b>some bold</b> text");
        return ok(html);
    }

    public Result setContentTypeApplicationJson() {
        JsonNode json = Json.toJson(Arrays.asList("ZW", "ZA", "UK"));
        return ok(json);
    }

    public Result setCustomHeaders() {
        JsonNode json = Json.toJson(Arrays.asList("ZW", "ZA", "UK"));
        return ok(json)
                .withHeader("Custom-Header-Date-Time", LocalDateTime.now().toString())
                .withHeader("Custom-Header-Millis", String.valueOf(Instant.now().toEpochMilli()));
    }
}
