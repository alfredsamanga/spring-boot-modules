# ResiliaTech Modules
This repository contains code for the articles on [resiliatech.com](https://resiliatech.com/).

This module is linked to the following article: 
[How To Manipulate HTTP Results in Play](https://resiliatech.com/http-results-play-framework)
