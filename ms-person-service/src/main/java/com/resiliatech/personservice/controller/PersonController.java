package com.resiliatech.personservice.controller;

import com.resiliatech.personservice.service.PersonService;
import com.resiliatech.sharedlib.dto.PersonDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/people")
public class PersonController {
    private final PersonService personService;

    @GetMapping
    public ResponseEntity<List<PersonDTO>> listPeople() {
        return new ResponseEntity<>(personService.list(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<PersonDTO> getPersonById(@PathVariable("id") Long id) {
        return personService.getById(id)
                .map(personDTO -> new ResponseEntity<>(personDTO, HttpStatus.OK))
                .orElse(ResponseEntity.notFound().build());
    }
}
