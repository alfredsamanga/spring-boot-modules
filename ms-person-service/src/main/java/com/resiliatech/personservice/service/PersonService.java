package com.resiliatech.personservice.service;

import com.resiliatech.sharedlib.dto.PersonDTO;

import java.util.List;
import java.util.Optional;

public interface PersonService {
    List<PersonDTO> list();
    Optional<PersonDTO> getById(Long id);
}
