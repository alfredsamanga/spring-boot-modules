package com.resiliatech.personservice.service;
import com.google.common.collect.Lists;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.resiliatech.sharedlib.dto.CarDTO;
import com.resiliatech.sharedlib.dto.PersonDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class PersonServiceImpl implements PersonService {
    @LoadBalanced
    private final RestTemplate restTemplate;
    private final ResourceLoader resourceLoader;
    private final ObjectMapper objectMapper;

    private static Map<Long, PersonDTO> peopleMap;

    @PostConstruct
    public void init() throws IOException {
        final Resource resource = resourceLoader.getResource("classpath:people.json");
        final List<PersonDTO> personDTOS = objectMapper.readValue(resource.getInputStream(), new TypeReference<List<PersonDTO>>() {});
        peopleMap = personDTOS.stream().collect(Collectors.toMap(PersonDTO::getId, Function.identity()));
    }

    @Override
    public List<PersonDTO> list() {
        return new ArrayList<>(peopleMap.values());
    }

    @Override
    @HystrixCommand(fallbackMethod = "getPersonByIdFallback")
    public Optional<PersonDTO> getById(Long id) {
        final PersonDTO personDTO = peopleMap.get(id);
        if(personDTO == null) return Optional.empty();
        final ResponseEntity<List<CarDTO>> responseEntity = restTemplate.exchange(
                "http://car-service/cars/owner/" + id,
                HttpMethod.GET,
                HttpEntity.EMPTY,
                new ParameterizedTypeReference<List<CarDTO>>() {});
        if(responseEntity.getStatusCode() == HttpStatus.OK) {
            personDTO.setCars(responseEntity.getBody());
        }
        return Optional.ofNullable(personDTO);
    }

    public Optional<PersonDTO> getPersonByIdFallback(Long id) {
        final PersonDTO personDTO = new PersonDTO();
        personDTO.setId(1000L);
        personDTO.setFirstName("Fall");
        personDTO.setLastName("Back");
        personDTO.setCars(Lists.newArrayList());
        return Optional.of(personDTO);
    }
}
