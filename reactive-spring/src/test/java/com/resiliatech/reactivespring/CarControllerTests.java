package com.resiliatech.reactivespring;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
public class CarControllerTests {
    @MockBean
    private CarRepository carRepository;

    private Car[] cars;

    @BeforeEach
    public void init() {
        cars = new Car[]{getFirstCar(), getSecondCar()};
    }

    @AfterEach
    public void destroy() throws Exception {
        cars = null;
    }

    @Test
    public void givenCar_whenCreate_thenShouldReturnCarAndCorrectStatus() throws Exception {
        WebTestClient webTestClient = WebTestClient.bindToController(new CarController(carRepository)).build();

        Mono<Car> carMono = Mono.just(cars[0]);
        when(carRepository.save(any())).thenReturn(carMono);

        webTestClient.post()
                .uri("/cars")
                .contentType(MediaType.APPLICATION_JSON)
                .body(carMono, Car.class)
                .exchange()
                .expectStatus().isCreated()
                .expectBody(Car.class)
                .isEqualTo(cars[0]);
    }

    @Test
    public void givenExistingCar_whenGet_thenShouldReturnCar() throws Exception {
        when(carRepository.findById(any())).thenReturn(Mono.just(cars[0]));
        WebTestClient webTestClient = WebTestClient.bindToController(new CarController(carRepository)).build();

        webTestClient.get().uri("/cars/1")
                .exchange()
                .expectStatus().isOk()
                .expectBody(Car.class)
                .isEqualTo(cars[0]);
    }

    @Test
    public void givenNonExistingCar_whenGet_thenShouldReturnNotFoundStatus() throws Exception {
        WebTestClient webTestClient = WebTestClient.bindToController(new CarController(carRepository)).build();

        webTestClient.get().uri("/cars/3")
                .exchange()
                .expectStatus().isOk()
                .expectBody().isEmpty();
    }

    @Test
    public void givenExistingCars_whenGet_thenShouldReturnCarCollection() throws Exception {
        when(carRepository.findAll()).thenReturn(Flux.fromArray(cars));
        WebTestClient webTestClient = WebTestClient.bindToController(new CarController(carRepository)).build();

        webTestClient.get().uri("/cars")
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$").isArray()
                .jsonPath("$").isNotEmpty()
                .jsonPath("$[0].name").isEqualTo(cars[0].getName())
                .jsonPath("$[0].make").isEqualTo(cars[0].getMake())
                .jsonPath("$[0].model").isEqualTo(cars[0].getModel())
                .jsonPath("$[0].year").isEqualTo(cars[0].getYear())
                .jsonPath("$[1].name").isEqualTo(cars[1].getName())
                .jsonPath("$[1].make").isEqualTo(cars[1].getMake())
                .jsonPath("$[1].model").isEqualTo(cars[1].getModel())
                .jsonPath("$[1].year").isEqualTo(cars[1].getYear());
    }

    @Test
    public void givenNonExistentCars_whenGet_thenShouldReturnEmptyCarCollection() throws Exception {
        WebTestClient webTestClient = WebTestClient.bindToController(new CarController(carRepository)).build();

        webTestClient.get().uri("/cars")
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$").isEmpty();
    }

    @Test
    public void givenCar_whenUpdate_thenShouldReturnUpdateCarAndCorrectStatus() throws Exception {
        WebTestClient webTestClient = WebTestClient.bindToController(new CarController(carRepository)).build();

        Mono<Car> carMono = Mono.just(cars[0]);
        when(carRepository.update(any())).thenReturn(carMono);

        webTestClient.put()
                .uri("/cars")
                .contentType(MediaType.APPLICATION_JSON)
                .body(carMono, Car.class)
                .exchange()
                .expectBody(Car.class)
                .isEqualTo(cars[0]);
    }

    @Test
    public void givenNonExistentCar_whenUpdate_thenShouldReturn404NotFoundStatus() throws Exception {
        WebTestClient webTestClient = WebTestClient.bindToController(new CarController(carRepository)).build();

        Mono<Car> mono = Mono.just(cars[0]);
        when(carRepository.update(any())).thenReturn(Mono.empty());

        webTestClient.put()
                .uri("/cars")
                .contentType(MediaType.APPLICATION_JSON)
                .body(mono, Car.class)
                .exchange()
                .expectBody().isEmpty();
    }

    @Test
    public void givenExistingCar_whenDelete_thenShouldReturnDeletedCarAndCorrectStatus() throws Exception {
        when(carRepository.delete(any())).thenReturn(Mono.just(cars[0]));
        WebTestClient webTestClient = WebTestClient.bindToController(new CarController(carRepository)).build();

        webTestClient.delete().uri("/cars/1")
                .exchange()
                .expectStatus().isOk()
                .expectBody(Car.class)
                .isEqualTo(cars[0]);
    }

    @Test
    public void givenNonExistentCar_whenDelete_thenShouldReturnCorrectStatus() throws Exception {
        WebTestClient webTestClient = WebTestClient.bindToController(new CarController(carRepository)).build();

        webTestClient.delete().uri("/cars/4")
                .exchange()
                .expectStatus().isOk()
                .expectBody().isEmpty();
    }

    private Car getFirstCar() {
        Car car = new Car();
        car.setId(1L);
        car.setName("Mazda Car");
        car.setMake("Mazda");
        car.setModel("Axela");
        car.setYear(2005);
        return car;
    }

    private Car getSecondCar() {
        Car car = new Car();
        car.setId(2L);
        car.setName("My Toyota");
        car.setMake("Toyota");
        car.setModel("Avensis");
        car.setYear(2010);
        return car;
    }
}
