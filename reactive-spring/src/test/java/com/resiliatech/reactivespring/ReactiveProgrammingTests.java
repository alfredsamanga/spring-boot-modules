package com.resiliatech.reactivespring;

import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.time.Duration;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

public class ReactiveProgrammingTests {
    private String[] words = {"The ", "quick ", "brown ", "fox ", "jumped ", "over ", "the ", "lazy ", "sleeping ", "dog"};
    private List<String> wordsList = Arrays.asList(words);
    private Stream<String> wordStream = Stream.of(words);

    @Test
    public void createFluxUsingJust() {
        final Flux<String> fluxOfWords = Flux.just("The ", "quick ", "brown ", "fox ", "jumped ", "over ", "the ", "lazy ", "sleeping ", "dog");

        verifyFlux(fluxOfWords);

        fluxOfWords.subscribe(word -> {
            System.out.printf("%s ", word);
        });
    }

    @Test
    public void arrayBasedFlux_withFromArray() {
        final Flux<String> wordFlux = Flux.fromArray(words);
        verifyFlux(wordFlux);
    }

    @Test
    public void listBasedFlux_withFromIterable() {
        final Flux<String> wordFlux = Flux.fromIterable(wordsList);
        verifyFlux(wordFlux);
    }

    @Test
    public void streamBasedFlux_withStream() {
        final Flux<String> wordFlux = Flux.fromStream(wordStream);
        verifyFlux(wordFlux);
    }

    @Test
    public void rangeBasedFlux_withRange() {
        final Flux<Integer> integerFlux = Flux.range(1, 3);
        verifyIntegerFlux(integerFlux);
    }

    @Test
    public void intervalBasedFlux_withInterval() {
        final Flux<Long> longFlux = Flux.interval(Duration.ofMillis(500)).take(3);
        verifyLongFlux(longFlux);
    }

    @Test
    public void zippedFluxes_withZip() {
        final Flux<Integer> integerFlux = Flux.range(1, 10);
        final Flux<String> wordFlux = Flux.fromArray(words);
        final Flux<String> numberedWords = integerFlux.zipWith(wordFlux)
                .map(tuple -> tuple.getT1() + ". " + tuple.getT2());
        verifyCountWordFlux(numberedWords);
    }

    @Test
    public void givenFlux_skipNElements() {
        Flux<String> skipFlux = Flux.just(words).skip(5);
        StepVerifier.create(skipFlux)
                .expectNext("over ", "the ", "lazy ", "sleeping ", "dog")
                .verifyComplete();
    }

    @Test
    public void givenFlux_skipSecondsDuration() {
        Flux<String> skipFlux = Flux.just(words).delayElements(Duration.ofSeconds(2))
                .skip(Duration.ofSeconds(6));
        StepVerifier.create(skipFlux)
                .expectNext("brown ", "fox ", "jumped ", "over ", "the ", "lazy ", "sleeping ", "dog")
                .verifyComplete();
    }

    @Test
    public void givenFlux_takeNElements() {
        Flux<String> nationalParkFlux = Flux.just(words).take(3);
        StepVerifier.create(nationalParkFlux).expectNext("The ", "quick ", "brown ").verifyComplete();
    }

    private void verifyIntegerFlux(Flux<Integer> integerFlux) {
        StepVerifier.create(integerFlux)
                .expectNext(1)
                .expectNext(2)
                .expectNext(3)
                .verifyComplete();
    }

    private void verifyLongFlux(Flux<Long> longFlux) {
        StepVerifier.create(longFlux)
                .expectNext(0L)
                .expectNext(1L)
                .expectNext(2L)
                .verifyComplete();
    }

    private void verifyFlux(Flux<String> wordFlux) {
        StepVerifier.create(wordFlux)
                .expectNext("The ")
                .expectNext("quick ")
                .expectNext("brown ")
                .expectNext("fox ")
                .expectNext("jumped ")
                .expectNext("over ")
                .expectNext("the ")
                .expectNext("lazy ")
                .expectNext("sleeping ")
                .expectNext("dog")
                .verifyComplete();
    }

    private void verifyCountWordFlux(Flux<String> wordFlux) {
        StepVerifier.create(wordFlux)
                .expectNext("1. The ")
                .expectNext("2. quick ")
                .expectNext("3. brown ")
                .expectNext("4. fox ")
                .expectNext("5. jumped ")
                .expectNext("6. over ")
                .expectNext("7. the ")
                .expectNext("8. lazy ")
                .expectNext("9. sleeping ")
                .expectNext("10. dog")
                .verifyComplete();
    }
}
