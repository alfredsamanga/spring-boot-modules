package com.resiliatech.reactivespring;

import org.reactivestreams.Publisher;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface CarRepository {
    Mono<Car> save(Mono<Car> carMono);
    Mono<Car> update(Mono<Car> carMono);
    Mono<Car> findById(Long id);
    Flux<Car> findAll();
    Mono<Car> delete(Long id);
}
