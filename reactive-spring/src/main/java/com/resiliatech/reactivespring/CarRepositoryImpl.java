package com.resiliatech.reactivespring;

import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.HashMap;
import java.util.Map;

@Component
public class CarRepositoryImpl implements CarRepository {
    private Map<Long, Car> carMap = new HashMap<>();

    @Override
    public Mono<Car> save(Mono<Car> carMono) {
        return carMono.map(this::saveHelper);
    }

    private Car saveHelper(Car car) {
        carMap.put(car.getId(), car);
        return car;
    }

    @Override
    public Mono<Car> update(Mono<Car> carMono) {
        return carMono.map(c -> {
            Car car = carMap.get(c.getId());
            if (car != null) {
                car.setName(c.getName());
                car.setMake(c.getMake());
                car.setModel(c.getModel());
                car.setYear(c.getYear());
                return car;
            } else {
                return saveHelper(c);
            }
        });
    }

    @Override
    public Mono<Car> findById(Long id) {
        final Car car = carMap.get(id);
        if (car == null) return Mono.empty();
        return Mono.just(car);
    }

    @Override
    public Flux<Car> findAll() {
        return Flux.fromIterable(carMap.values());
    }

    @Override
    public Mono<Car> delete(Long id) {
        final Car removedCar = carMap.remove(id);
        if (removedCar == null) return Mono.empty();
        return Mono.just(removedCar);
    }
}
