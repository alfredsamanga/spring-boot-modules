package com.resilitech.lagomsetup.api;

import akka.NotUsed;
import com.lightbend.lagom.javadsl.api.Descriptor;
import com.lightbend.lagom.javadsl.api.Service;
import com.lightbend.lagom.javadsl.api.ServiceCall;

import static com.lightbend.lagom.javadsl.api.Service.*;

public interface LagomSetupService extends Service {
    ServiceCall<NotUsed, String> helloWorld();

    @Override
    default Descriptor descriptor() {
        return named("lagomSetup").withCalls(
                        pathCall("/api/hello/world", this::helloWorld)
                ).withAutoAcl(true);
    }
}
