package com.resilitech.lagomsetup.impl;

import akka.NotUsed;
import com.lightbend.lagom.javadsl.api.ServiceCall;
import com.resilitech.lagomsetup.api.LagomSetupService;


import static java.util.concurrent.CompletableFuture.completedFuture;

/**
 * Implementation of the LagomSetupService.
 */
public class LagomSetupServiceImpl implements LagomSetupService {
    @Override
    public ServiceCall<NotUsed, String> helloWorld() {
        return request -> {
            return completedFuture("This is our first Lagom Microservice");
        };
    }
}
