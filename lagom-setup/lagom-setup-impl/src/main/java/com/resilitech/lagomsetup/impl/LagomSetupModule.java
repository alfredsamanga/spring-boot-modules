package com.resilitech.lagomsetup.impl;

import com.google.inject.AbstractModule;
import com.lightbend.lagom.javadsl.server.ServiceGuiceSupport;

import com.resilitech.lagomsetup.api.LagomSetupService;

/**
 * The module that binds the LagomSetupService so that it can be served.
 */
public class LagomSetupModule extends AbstractModule implements ServiceGuiceSupport {
    @Override
    protected void configure() {
        bindService(LagomSetupService.class, LagomSetupServiceImpl.class);
    }
}
