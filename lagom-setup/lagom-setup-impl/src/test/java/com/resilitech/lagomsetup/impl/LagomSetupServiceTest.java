package com.resilitech.lagomsetup.impl;

import org.junit.Test;

import com.resilitech.lagomsetup.api.LagomSetupService;

import static com.lightbend.lagom.javadsl.testkit.ServiceTest.defaultSetup;
import static com.lightbend.lagom.javadsl.testkit.ServiceTest.withServer;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.junit.Assert.assertEquals;

public class LagomSetupServiceTest {
    @Test
    public void shouldStorePersonalizedGreeting() {
        withServer(defaultSetup().withCassandra(), server -> {
            LagomSetupService service = server.client(LagomSetupService.class);

            String msg1 = service.helloWorld().invoke().toCompletableFuture().get(5, SECONDS);
            assertEquals("This is our first Lagom Microservice", msg1);
        });
    }
}
