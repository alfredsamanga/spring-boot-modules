# ResiliaTech Modules
This repository contains code for the articles on [resiliatech.com](https://resiliatech.com/).

This module is linked to the following article: 
[How to Set up a Lagom Framework Application](https://resiliatech.com/lagom-framework-setup)
