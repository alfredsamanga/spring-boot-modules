package com.resiliatech.springbootsetup;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootSetupApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootSetupApplication.class, args);
	}

}
