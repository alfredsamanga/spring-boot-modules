# ResiliaTech Modules
This repository contains code for the articles on [resiliatech.com](https://resiliatech.com/).

This module is linked to the following article:
[Setting up a Spring Boot Application for Absolute Beginners](https://resiliatech.com/setting-up-spring-boot/)
