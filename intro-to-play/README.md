# ResiliaTech Modules
This repository contains code for the articles on [resiliatech.com](https://resiliatech.com/).

This module is linked to the following article: 
[Setting up a Play Framework Application](https://resiliatech.com/play-framework-setup)
