# ResiliaTech Modules
This repository contains code for the articles on [resiliatech.com](https://resiliatech.com/).

We have the following articles on Spring Boot so far:
1. [Setting up a Spring Boot Application for Absolute Beginners](https://resiliatech.com/setting-up-spring-boot/)
2. [REST APIs With Spring Boot](https://resiliatech.com/rest-api-spring-boot/)
3. [Security with Spring Boot Applications](https://resiliatech.com/springboot-security/)
4. [Thymeleaf Templates With Spring Boot](https://resiliatech.com/thymeleaf-spring-boot/)
5. [Microservice Applications With Spring Boot](https://resiliatech.com/spring-boot-microservices/)
6. [Reactive Programming With Spring Boot](https://resiliatech.com/reactive-spring-boot/)
7. [Websockets With Spring Boot](https://resiliatech.com/websockets-with-spring-boot)
8. [Consuming Restful Webservices in Spring Boot](https://resiliatech.com/consuming-rest-with-spring-boot)
9. [Deploying a Spring Boot Application](https://resiliatech.com/spring-boot-deployment)
10. [Handling User Input With Spring Boot and Thymeleaf](https://resiliatech.com/thymeleaf-user-input-spring-boot-forms)
11. [Relational Databases With Spring Boot](https://resiliatech.com/relational-db-spring-boot-and-spring-data-jpa)
12. [Running Applications in Kubernetes With Pods](https://resiliatech.com/running-applications-in-kubernetes-with-pods)
13. [Setting up a Play Framework Application](https://resiliatech.com/play-framework-setup)
14. [Introduction to Kubernetes for Absolute Beginners](https://resiliatech.com/intro-kubernetes)
15. [REST APIs With Play Framework](https://resiliatech.com/rest-api-play-framework)
16. [Kubernetes Namespaces, Labels and Selectors](https://resiliatech.com/kubernetes-namespaces-labels-and-selectors)
17. [Asynchronous Programming in a Play Framework Application](https://resiliatech.com/asynchronous-play-framework)
18. [Accessing Relational Databases With Play Framework](https://resiliatech.com/rdbms-play-framework)
19. [Exposing Pods Using Services – Kubernetes](https://resiliatech.com/exposing-pods-using-services-kubernetes)
20. [How To Use Twirl Templates in Play Framework](https://resiliatech.com/twirl-templates-play-framework)
21. [How to Run Apps With Kubernetes Deployments](https://resiliatech.com/running-apps-with-kubernetes-deployments)
22. [How To Handle User Input With Play Framework](https://resiliatech.com/user-input-play-framework)
23. [How To Manipulate HTTP Results in Play](https://resiliatech.com/http-results-play-framework)
24. [How to Set up a Lagom Framework Application](https://resiliatech.com/lagom-framework-setup)
25. [How to Get Into Data Science or Data Analytics](https://resiliatech.com/how-to-get-into-data-science-or-data-analytics/)
26. [The Difference Between Data Science, Data Analytics and Big Data](https://resiliatech.com/differences-between-data-science-data-analytics-and-big-data/)
27. [How to Implement LDAP Authentication With Spring Boot](https://resiliatech.com/how-to-implement-ldap-authentication-with-spring-boot/)

To run each spring boot module, open the module root directory and run the following command:
```
mvn spring-boot:run
```
Please follow instructions in the article for how to run the other non-spring-boot modules.
