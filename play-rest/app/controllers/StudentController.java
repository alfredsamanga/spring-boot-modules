package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import model.Student;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import services.api.StudentService;
import utils.Util;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;

public class StudentController extends Controller {
    private StudentService studentService;

    @Inject
    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    public Result createStudent(Http.Request request) {
        final JsonNode jsonNode = request.body().asJson();
        final Student student = Util.mapToObject(jsonNode, Student.class);
        final Optional<Student> studentOptional = studentService.create(student);
        return studentOptional.map(s -> ok(Util.createResponse(s, true)))
                .orElse(internalServerError(Util.createResponse("Student not found", false)));
    }

    public Result updateStudent(Http.Request request) {
        final JsonNode jsonNode = request.body().asJson();
        final Student student = Util.mapToObject(jsonNode, Student.class);
        final Optional<Student> studentOptional = studentService.update(student);
        return studentOptional.map(s -> ok(Util.createResponse(s, true)))
                .orElse(notFound(Util.createResponse("Student not found", false)));
    }

    public Result getStudentById(Http.Request request, String studentId) {
        final Optional<Student> studentOptional = studentService.getById(studentId);
        return studentOptional.map(student -> ok(Util.createResponse(student, true)))
                .orElse(notFound(Util.createResponse("Student not found", false)));
    }

    public Result deleteStudentById(Http.Request request, String studentId) {
        final Optional<Student> studentOptional = studentService.delete(studentId);
        return studentOptional.map(student -> ok(Util.createResponse(student, true)))
                .orElse(notFound(Util.createResponse("Student not found", false)));
    }

    public Result getStudents(Http.Request request) {
        final List<Student> students = studentService.getAll();
        return ok(Util.createResponse(students, true));
    }

}
