package services.impl;

import model.Student;
import repository.api.StudentRepository;
import services.api.StudentService;

import javax.inject.Inject;
import java.util.*;
import java.util.concurrent.CompletionStage;

public class StudentServiceImpl implements StudentService {

    private StudentRepository studentRepository;

    @Inject
    public StudentServiceImpl(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    @Override
    public Optional<Student> create(Student student) {
        return studentRepository.createStudent(student);
    }

    @Override
    public Optional<Student> getById(String studentId) {
        return studentRepository.getStudentById(studentId);
    }

    @Override
    public List<Student> getAll() {
        return studentRepository.getAll();
    }

    @Override
    public Optional<Student> update(Student student) {
        return studentRepository.updateStudent(student);
    }

    @Override
    public Optional<Student> delete(String studentId) {
        return studentRepository.deleteStudentById(studentId);
    }
}
