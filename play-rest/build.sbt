name := """play-rest"""
organization := "com.resiliatech"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.13.3"

libraryDependencies ++= Seq(
  guice,
  "org.projectlombok" % "lombok" % "1.18.16" % "provided",
)
