# ResiliaTech Modules
This repository contains code for the articles on [resiliatech.com](https://resiliatech.com/).

This module is linked to the following article: 
[REST APIs With Play Framework](https://resiliatech.com/rest-api-play-framework)
