package data;

import akka.serialization.jackson.Compression;

import java.time.OffsetDateTime;
import java.time.temporal.ChronoUnit;

public class Student {
    private String firstName;
    private String lastName;
    private OffsetDateTime dateOfBirth;

    public Student() {
    }

    public Student(String firstName, String lastName, OffsetDateTime dateOfBirth) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public OffsetDateTime getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(OffsetDateTime dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public long getAge() {
        return ChronoUnit.YEARS.between(dateOfBirth, OffsetDateTime.now());
    }
}
