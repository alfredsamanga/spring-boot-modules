package controllers;

import data.Student;
import play.mvc.*;

import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class HomeController extends Controller {
    public Result index() {
        Student student = new Student("John", "Doe", OffsetDateTime.now());
        List<Student> studentList = getStudents();
        return ok(views.html.index.render(student, studentList, DateTimeFormatter.ofPattern("dd MMM yyyy HH:mm")));
    }

    private List<Student> getStudents() {
        return List.of(
                new Student("Alfred", "Samanga", OffsetDateTime.now().minusYears(15)),
                new Student("Anotida", "Maditsha", OffsetDateTime.now().minusYears(22))
        );
    }
}
