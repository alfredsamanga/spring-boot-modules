# ResiliaTech Modules
This repository contains code for the articles on [resiliatech.com](https://resiliatech.com/).

This module is linked to the following article: 
[How To Use Twirl Templates in Play Framework](https://resiliatech.com/twirl-templates-play-framework)
