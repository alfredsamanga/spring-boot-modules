package com.resiliatech.propsserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@EnableConfigServer
@SpringBootApplication
public class PropsServerApplication {
	public static void main(String[] args) {
		SpringApplication.run(PropsServerApplication.class, args);
	}
}
