package com.resiliatech.springbootldapauth.models;

import lombok.Data;
import org.springframework.ldap.odm.annotations.Attribute;
import org.springframework.ldap.odm.annotations.Entry;
import org.springframework.ldap.odm.annotations.Id;

import javax.naming.Name;

@Data
@Entry(base = "ou=users", objectClasses = {
        "person",
        "inetOrgPerson",
        "organizationalPerson",
        "top"
})
public class LdapUser {
    @Id
    private Name dn;

    @Attribute(name = "cn")
    private String username;

    @Attribute(name = "sn")
    private String password;
}
