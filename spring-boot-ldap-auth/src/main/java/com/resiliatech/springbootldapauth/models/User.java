package com.resiliatech.springbootldapauth.models;

import lombok.Data;
import lombok.RequiredArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@RequiredArgsConstructor
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String username;
    private transient String password;

    private String firstName;
    private String lastName;
    private String email;
    private boolean enabled;
    private boolean tokenExpired;

}

