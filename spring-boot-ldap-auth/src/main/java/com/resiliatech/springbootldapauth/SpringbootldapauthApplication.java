package com.resiliatech.springbootldapauth;

import com.resiliatech.springbootldapauth.configs.LDAPConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import(value = {LDAPConfig.class})
@ComponentScan("com.resiliatech.springbootldapauth.*")
public class SpringbootldapauthApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootldapauthApplication.class, args);
	}

}
