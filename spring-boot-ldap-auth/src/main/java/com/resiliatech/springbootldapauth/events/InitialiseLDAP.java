package com.resiliatech.springbootldapauth.events;

import com.resiliatech.springbootldapauth.models.LdapUser;
import com.resiliatech.springbootldapauth.models.User;
import com.resiliatech.springbootldapauth.services.iface.LdapUserService;
import com.resiliatech.springbootldapauth.services.iface.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;


@Component
public class InitialiseLDAP implements ApplicationListener<ContextRefreshedEvent> {
    @Autowired
    private LdapUserService ldapUserService;

    @Value("${user.username}")
    private String username;

    @Value("${user.password}")
    private String password;

    @Autowired
    private UserService userService;

    private boolean initialised;

    @Override
    @Transactional
    public void onApplicationEvent(ContextRefreshedEvent event) {//Initialisation should only run once
        if (ldapUserService.rdnExists("users") || initialised) return;

        ldapUserService.createOU("users");

        //Saved in LDAP
        LdapUser ldapUser = new LdapUser();
        ldapUser.setUsername(username);
        ldapUser.setPassword(password);
        ldapUser.setDn(ldapUserService.createLdapName(username));
        ldapUserService.create(ldapUser);

        //Saved in Postgres DB
        User user = new User();
        user.setUsername(username);
        user.setFirstName("Admin");
        user.setLastName("Admin");
        user.setEmail("test@test.com");
        user.setEnabled(true);
        user.setTokenExpired(false);
        userService.createUser(user);

        initialised = true;
    }
}
