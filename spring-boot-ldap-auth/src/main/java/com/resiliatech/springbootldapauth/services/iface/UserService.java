package com.resiliatech.springbootldapauth.services.iface;

import com.resiliatech.springbootldapauth.models.User;

public interface UserService {
    User findByUsername(String name);

    User createUser(User user);
}
