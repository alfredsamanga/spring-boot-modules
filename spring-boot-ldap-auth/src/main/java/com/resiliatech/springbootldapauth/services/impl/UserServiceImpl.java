package com.resiliatech.springbootldapauth.services.impl;


import com.resiliatech.springbootldapauth.models.User;
import com.resiliatech.springbootldapauth.repositories.UserRepository;
import com.resiliatech.springbootldapauth.services.iface.LdapUserService;
import com.resiliatech.springbootldapauth.services.iface.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private LdapUserService ldapUserService;

    @Override
    public User findByUsername(String name) {
        List<User> users = userRepository.findByUsername(name);
        return !users.isEmpty() ? users.get(0) : null;
    }

    @Override
    public User createUser(User user) {
        return userRepository.save(user);
    }

}
