package com.resiliatech.springbootldapauth.services.impl;

import com.resiliatech.springbootldapauth.models.LdapUser;
import com.resiliatech.springbootldapauth.services.iface.LdapUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.ldap.core.AttributesMapper;
import org.springframework.ldap.core.DirContextAdapter;
import org.springframework.ldap.core.DirContextOperations;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.LdapContextSource;
import org.springframework.ldap.support.LdapNameBuilder;
import org.springframework.ldap.support.LdapUtils;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.naming.Name;
import javax.naming.directory.DirContext;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.List;

@Service
@Slf4j
public class LdapUserServiceImpl implements LdapUserService {

    @Autowired
    private LdapTemplate ldapTemplate;

    @Autowired
    private LdapContextSource contextSource;

    @Value("${ldap.partitionSuffix}")
    private String partitionSuffix;

    @Override
    public boolean authenticate(String username, String password) {
        DirContext ctx = null;
        try {
            ctx = contextSource.getContext("cn=" + username + ",ou=users," + partitionSuffix, password);
            return true;
        } catch (Exception e) {
            log.error("Authentication failure", e);
            return false;
        } finally {
            LdapUtils.closeContext(ctx);
        }
    }

    @Override
    public void create(LdapUser user) {
        if (!search(user.getUsername()).isEmpty()) return;
        DirContextAdapter context = new DirContextAdapter(user.getDn());
        context.setAttributeValues("objectclass", new String[]{"top", "person", "organizationalPerson", "inetOrgPerson"});
        context.setAttributeValue("cn", user.getUsername());
        context.setAttributeValue("sn", user.getUsername());
        context.setAttributeValue("userPassword", "{bcrypt}" + digestSHA256(user.getPassword()));
        ldapTemplate.bind(context);
    }

    @Override
    public Name createLdapName(String name) {
        return LdapNameBuilder
                .newInstance()
                .add("ou", "users")
                .add("cn", name)
                .build();
    }

    @Override
    public void modify(LdapUser user, final String[] attr) {
        Name dn = createLdapName(user.getUsername());
        DirContextOperations context = ldapTemplate.lookupContext(dn);

        context.setAttributeValues("objectclass", attr);
        context.setAttributeValue("cn", user.getUsername());
        context.setAttributeValue("sn", user.getUsername());
        context.setAttributeValue("userPassword",  digestSHA256(user.getPassword()));

        ldapTemplate.modifyAttributes(context);
    }

    @Override
    public List<String> search(final String username) {
        return ldapTemplate.search(
                "ou=users",
                "cn=" + username,
                (AttributesMapper<String>) attrs -> (String) attrs
                        .get("cn")
                        .get());
    }

    @Override
    public boolean rdnExists(String rdn) {
        try {
            ldapTemplate.lookup("ou=" + rdn);
            return true;
        } catch (org.springframework.ldap.NamingException ne) {
            return false;
        }
    }

    @Override
    public void createOU(String name) {
        DirContextAdapter context = new DirContextAdapter(LdapNameBuilder
                .newInstance()
                .add("ou", name)
                .build());
        context.setAttributeValues("objectclass", new String[]{"top", "organizationalUnit"});
        context.setAttributeValue("ou", name);

        ldapTemplate.bind(context);
    }

    private String digestSHA256(String password) {
        String base64;
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            digest.update(password.getBytes(StandardCharsets.UTF_8));
            base64 = Base64
                    .getEncoder()
                    .encodeToString(digest.digest());
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
        return "{SHA-256}" + base64;
    }
}
