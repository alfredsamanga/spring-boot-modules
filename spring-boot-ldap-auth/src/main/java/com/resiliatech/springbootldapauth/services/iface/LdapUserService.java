package com.resiliatech.springbootldapauth.services.iface;


import com.resiliatech.springbootldapauth.models.LdapUser;

import javax.naming.Name;
import java.util.List;

public interface LdapUserService {
    boolean authenticate(String username, String password);
    void create(LdapUser user);
    void modify(LdapUser user, final String[] attr);
    Name createLdapName(String name);
    List<String> search(String u);
    boolean rdnExists(String rdn);
    void createOU(String name);
}
