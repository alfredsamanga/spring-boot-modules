package com.resiliatech.springbootldapauth.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Slf4j
@Controller
public class MainController {
    @GetMapping(value = "/login")
    String login() {
        return "auth/login";
    }

    @GetMapping(value = "/")
    String loggedIn() {
        return "home/home";
    }
}
