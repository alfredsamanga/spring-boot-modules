package com.resiliatech.springbootldapauth.security;

import com.resiliatech.springbootldapauth.services.iface.LdapUserService;
import com.resiliatech.springbootldapauth.services.iface.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Slf4j
@Component
public class LDAPAuthenticationProvider implements AuthenticationProvider {
    @Autowired
    private LdapUserService ldapUserService;

    @Autowired
    private UserService userService;

    @Override
    public Authentication authenticate(Authentication authentication)
            throws AuthenticationException {
        String name = authentication.getName();
        String password = authentication.getCredentials().toString();

        if (ldapUserService.authenticate(name, password)) {
            //This is where you get the user info from db and get additional info like Role and Access rights etc
            //User user = userService.findByUsername(name);
            return new UsernamePasswordAuthenticationToken(
                    name,
                    password,
                    Arrays.asList(new SimpleGrantedAuthority(
                            "ROLE_ADMIN"),
                            new SimpleGrantedAuthority("ROLE_USER")
                    )
            );
        } else {
            throw new BadCredentialsException("Authentication failed");
        }
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}
