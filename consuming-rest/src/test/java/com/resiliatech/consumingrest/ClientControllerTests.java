package com.resiliatech.consumingrest;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
public class ClientControllerTests {

    @Autowired
    private ClientController clientController;

    private MockMvc mockMvc;

    @BeforeEach
    public void setup() {
        this.mockMvc = MockMvcBuilders.
                standaloneSetup(this.clientController).build();
    }

    @Test
    public void givenSingleCommentUrl_whenGet_shouldReturnComment() throws Exception {
        mockMvc.perform(get("/api/comments/{id}", 1))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name").value("id labore ex et quam laborum"));
    }

    @Test
    public void givenSingleCommentUrl_whenGetHeader_shouldReturnCommentHeaders() throws Exception {
        mockMvc.perform(get("/api/comments/headers/{id}", 1))
                .andExpect(status().isOk());
    }

    @Test
    public void givenSingleCommentUrl_whenGetOptionsForAllow_shouldReturnCommentOptionsForAllow() throws Exception {
        mockMvc.perform(get("/api/comments/options-for-allow/{id}", 1))
                .andExpect(status().isOk());
    }

    @Test
    public void givenSingleCommentUrlAndMapVariables_whenGet_shouldReturnComment() throws Exception {
        mockMvc.perform(get("/api/comments/with-map/{id}", 1))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name").value("id labore ex et quam laborum"));
    }

    @Test
    public void givenSingleCommentUrlAndMapVariables_whenGetWithUri_shouldReturnComment() throws Exception {
        mockMvc.perform(get("/api/comments/with-uri/{id}", 1))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name").value("id labore ex et quam laborum"));
    }

    @Test
    public void givenSingleCommentUrlAndMapVariables_whenGetForEntity_shouldReturnCommentWithHeaders() throws Exception {
        mockMvc.perform(get("/api/comments/with-response-entity/{id}", 1))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(header().exists("Content-type"))
                .andExpect(jsonPath("$.name").value("id labore ex et quam laborum"));
    }

    @Test
    public void givenSingleComment_whenUpdate_shouldReturnNothing() throws Exception {
        mockMvc.perform(
                put("/api/comments")
                .contentType(MediaType.APPLICATION_JSON)
                .content(getCommentJson(getComment(1))))
                .andExpect(status().isOk());
    }

    @Test
    public void givenSingleComment_whenCreate_shouldReturnCreatedComment() throws Exception {
        mockMvc.perform(
                post("/api/comments")
                .contentType(MediaType.APPLICATION_JSON)
                .content(getCommentJson(getComment(1))))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(header().exists("Content-type"))
                .andExpect(jsonPath("$.name").value("id labore ex et quam laborum"));
    }

    @Test
    public void givenSingleComment_whenPostForLocation_shouldReturnCreatedCommentLocation() throws Exception {
        mockMvc.perform(
                post("/api/comments/for/location")
                .contentType(MediaType.APPLICATION_JSON)
                .content(getCommentJson(getComment(1))))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(header().exists("Content-type"));
    }

    @Test
    public void givenSingleComment_whenPostForEntity_shouldReturnCreatedCommentLocation() throws Exception {
        mockMvc.perform(
                post("/api/comments/for/entity")
                .contentType(MediaType.APPLICATION_JSON)
                .content(getCommentJson(getComment(1))))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(header().exists("Content-type"))
                .andExpect(jsonPath("$.name").value("id labore ex et quam laborum"));
    }

    private String getCommentJson(Comment comment) {
        return new StringBuilder()
                .append("{").append("\"postId\": ")
                .append(comment.getPostId()).append(",\n").append("    \"id\": ")
                .append(comment.getId()).append(",\n").append("    \"name\": \"")
                .append(comment.getName()).append("\",\n").append("    \"email\": \"")
                .append(comment.getEmail()).append("\",\n").append("    \"body\": \"")
                .append(comment.getBody()).append("\"\n").append("  }").toString();
    }

    private Comment getComment(int id) {
        final Comment comment = new Comment();
        comment.setId(id);
        comment.setPostId(1);
        comment.setName("id labore ex et quam laborum");
        comment.setEmail("Eliseo@gardner.biz");
        comment.setBody("laudantium enim quasi est quidem magnam voluptate ipsam eos\\ntempora quo necessitatibus\\ndolor quam autem quasi\\nreiciendis et nam sapiente accusantium");
        return comment;
    }

    @Test
    public void givenSingleCommentId_whenDelete_shouldReturnNothing() throws Exception {
        mockMvc.perform(delete("/api/comments/{commentId}", 1))
                .andExpect(status().isOk());
    }

    @Test
    public void givenAllCommentUrl_whenGet_shouldReturnCommentsList() throws Exception {
        mockMvc.perform(get("/api/comments"))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(500)))
                .andExpect(jsonPath("$[0].name").value("id labore ex et quam laborum"));
    }
}
