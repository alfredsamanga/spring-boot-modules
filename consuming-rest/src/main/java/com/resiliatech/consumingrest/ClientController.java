package com.resiliatech.consumingrest;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class ClientController {
    private final RestTemplate restTemplate;
    private static final String BASE_URL = "https://jsonplaceholder.typicode.com/comments";

    @GetMapping("/comments/{commentId}")
    public Comment getCommentById(@PathVariable("commentId") String commentId) {
        return restTemplate.getForObject(BASE_URL + "/{id}", Comment.class, commentId);
    }

    private Comment getCommentBy2Id() {
        return restTemplate.getForObject("http://example.com/{id}/another/{arg2}/another/{arg3}", Comment.class, 1, 2, 3, 4);
    }

    @GetMapping("/comments/with-map/{commentId}")
    public Comment getCommentByIdWithMapArguments(@PathVariable("commentId") String commentId) {
        Map<String, String> urlVariables = new HashMap<>();
        urlVariables.put("id", commentId);
        return restTemplate.getForObject(BASE_URL + "/{id}", Comment.class, urlVariables);
    }

    @GetMapping("/comments/with-uri/{commentId}")
    public Comment getCommentByIdWithUri(@PathVariable("commentId") String commentId) {
        Map<String, String> urlVariables = new HashMap<>();
        urlVariables.put("id", commentId);
        URI url = UriComponentsBuilder
                .fromHttpUrl(BASE_URL + "/{id}")
                .build(urlVariables);
        return restTemplate.getForObject(url, Comment.class);
    }

    @GetMapping("/comments/with-response-entity/{commentId}")
    public ResponseEntity<Comment> getCommentByIdWithGetForEntity(@PathVariable("commentId") String commentId) {
        ResponseEntity<Comment> responseEntity = restTemplate.getForEntity(BASE_URL + "/{id}", Comment.class, commentId);
        final HttpStatus statusCode = responseEntity.getStatusCode();
        //check status code
        return responseEntity;
    }

    @GetMapping("/comments/with-response-entity-vars-map/{commentId}")
    public ResponseEntity<Comment> getCommentByIdWithGetForEntityAndVarMap(@PathVariable("commentId") String commentId) {
        Map<String, String> urlVariables = new HashMap<>();
        urlVariables.put("id", commentId);
        ResponseEntity<Comment> responseEntity = restTemplate.getForEntity(BASE_URL + "/{id}", Comment.class, urlVariables);
        final HttpStatus statusCode = responseEntity.getStatusCode();
        //check status code
        return responseEntity;
    }

    @GetMapping("/comments/with-response-entity-uri/{commentId}")
    public ResponseEntity<Comment> getCommentByIdWithGetForEntityAndURI(@PathVariable("commentId") String commentId) {
        Map<String, String> urlVariables = new HashMap<>();
        urlVariables.put("id", commentId);
        URI url = UriComponentsBuilder.fromHttpUrl(BASE_URL + "/{id}").build(urlVariables);
        ResponseEntity<Comment> responseEntity = restTemplate.getForEntity(url, Comment.class);
        final HttpStatus statusCode = responseEntity.getStatusCode();
        //check status code
        return responseEntity;
    }

    @GetMapping("/comments/headers/{commentId}")
    public void getCommentHeaders(@PathVariable("commentId") String commentId) {
        final HttpHeaders httpHeaders = restTemplate.headForHeaders(BASE_URL + "/{id}", commentId);
        httpHeaders.forEach((key, value) -> {
            System.out.printf("\n%20s\t%40s", key, value);
        });
        System.out.println();
    }

    @GetMapping("/comments/headers-with-map/{commentId}")
    public void getCommentHeadersWithMap(@PathVariable("commentId") String commentId) {
        Map<String, String> urlVariables = new HashMap<>();
        urlVariables.put("id", commentId);
        final HttpHeaders httpHeaders = restTemplate.headForHeaders(BASE_URL + "/{id}", urlVariables);
        httpHeaders.forEach((key, value) -> {
            System.out.printf("\n%20s\t%40s", key, value);
        });
        System.out.println();
    }

    @GetMapping("/comments/headers-with-uri-map/{commentId}")
    public void getCommentHeadersWithURIMap(@PathVariable("commentId") String commentId) {
        Map<String, String> urlVariables = new HashMap<>();
        urlVariables.put("id", commentId);
        URI url = UriComponentsBuilder.fromHttpUrl(BASE_URL + "/{id}").build(urlVariables);
        final HttpHeaders httpHeaders = restTemplate.headForHeaders(url);
        httpHeaders.forEach((key, value) -> {
            System.out.printf("\n%20s\t%40s", key, value);
        });
        System.out.println();
    }

    @GetMapping("/comments/options-for-allow/{commentId}")
    public void getCommentOptionsForAllow(@PathVariable("commentId") String commentId) {
        final Set<HttpMethod> httpMethods = restTemplate.optionsForAllow(BASE_URL + "/{id}", commentId);
        System.out.println("HTTP methods: " + httpMethods.size());
        httpMethods.forEach(System.out::println);
    }

    @GetMapping("/comments/options-for-allow-with-map/{commentId}")
    public void getCommentOptionsForAllowWithMap(@PathVariable("commentId") String commentId) {
        Map<String, String> urlVariables = new HashMap<>();
        urlVariables.put("id", commentId);
        final Set<HttpMethod> httpMethods = restTemplate.optionsForAllow(BASE_URL + "/{id}", urlVariables);
        httpMethods.forEach(System.out::println);
    }

    @GetMapping("/comments/options-for-allow-with-uri-map/{commentId}")
    public void getCommentOptionsForAllowWithURIMap(@PathVariable("commentId") String commentId) {
        Map<String, String> urlVariables = new HashMap<>();
        urlVariables.put("id", commentId);
        URI url = UriComponentsBuilder.fromHttpUrl(BASE_URL + "/{id}").build(urlVariables);
        final Set<HttpMethod> httpMethods = restTemplate.optionsForAllow(url);
        httpMethods.forEach(System.out::println);
    }

    @PutMapping("/comments")
    public void updateComment(@RequestBody Comment comment) {
        Map<String, String> urlVariables = new HashMap<>();
        urlVariables.put("id", comment.getId().toString());
        URI url = UriComponentsBuilder.fromHttpUrl(BASE_URL + "/{id}").build(urlVariables);
        restTemplate.put(url, comment);
        restTemplate.put(BASE_URL + "/{id}", comment, urlVariables);
        restTemplate.put(BASE_URL + "/{id}", comment, comment.getId());
    }

    @DeleteMapping("/comments/{commentId}")
    public void deleteComment(@PathVariable("commentId") String commentId) {
        Map<String, String> urlVariables = new HashMap<>();
        urlVariables.put("id", commentId);
        URI url = UriComponentsBuilder.fromHttpUrl(BASE_URL + "/{id}").build(urlVariables);
        restTemplate.delete(url);
        restTemplate.delete(BASE_URL + "/{id}", urlVariables);
        restTemplate.delete(BASE_URL + "/{id}", commentId);
    }

    @PostMapping("/comments")
    public Comment createComment(@RequestBody Comment comment) {
        String[] urlVars = new String[]{"1", "2", "3"};
        Map<String, String> urlVariables = new HashMap<>();
        URI url = UriComponentsBuilder.fromHttpUrl(BASE_URL).build(urlVariables);
        restTemplate.postForObject(url, comment, Comment.class);
        restTemplate.postForObject(BASE_URL, comment, Comment.class);
        restTemplate.postForObject(BASE_URL, comment, Comment.class, urlVars);
        restTemplate.postForObject(BASE_URL, comment, Comment.class, urlVariables);
        return restTemplate.postForObject(BASE_URL, comment, Comment.class);
    }

    @PostMapping("/comments/post-for-entity")
    public ResponseEntity<Comment> createCommentUsingPostForEntity(@RequestBody Comment comment) {
        String[] urlVars = new String[]{"1", "2", "3"};
        Map<String, String> urlVariables = new HashMap<>();
        URI url = UriComponentsBuilder.fromHttpUrl(BASE_URL).build(urlVariables);
        restTemplate.postForEntity(url, comment, Comment.class);
        restTemplate.postForEntity(BASE_URL, comment, Comment.class);
        restTemplate.postForEntity(BASE_URL, comment, Comment.class, urlVars);
        restTemplate.postForEntity(BASE_URL, comment, Comment.class, urlVariables);
        return restTemplate.postForEntity(BASE_URL, comment, Comment.class);
    }

    @PostMapping("/comments/post-for-entity/with-uri")
    public ResponseEntity<Comment> createCommentUsingPostForEntityWithUrl(@RequestBody Comment comment) {
        URI url = UriComponentsBuilder.fromHttpUrl(BASE_URL).build().toUri();
        return restTemplate.postForEntity(url, comment, Comment.class);
    }

    @PostMapping("/comments/for/location")
    public URI createCommentUsingPostForLocation(Comment comment) {
        final URI uri = restTemplate.postForLocation(BASE_URL, comment);
        log.info("Comment was created at: {}", uri.toString());
        return uri;
    }

    @PostMapping("/comments/for/entity")
    public Comment createCommentWithPostForEntity(@RequestBody Comment comment) {
        ResponseEntity<Comment> responseEntity = restTemplate.postForEntity(BASE_URL,
                comment,
                Comment.class);
        log.info("New resource created at " +
                responseEntity.getHeaders().getLocation());
        return responseEntity.getBody();
    }


    @GetMapping("/comments")
    public ResponseEntity<List<Comment>> getComments() {
        return restTemplate.exchange(
                BASE_URL,
                HttpMethod.GET,
                HttpEntity.EMPTY,
                new ParameterizedTypeReference<List<Comment>>() {}
        );
    }


    @GetMapping("/comments/example/get-requests")
    public ResponseEntity<List<Comment>> getCommentsListExamples() {
        Map<String, String> urlVariables = new HashMap<>();
        urlVariables.put("postId", "1");
        URI url = UriComponentsBuilder.fromHttpUrl(BASE_URL + "/posts/{postId}/comments").build(urlVariables);
        final ResponseEntity<List<Comment>> exchange = restTemplate.exchange(
                BASE_URL,
                HttpMethod.GET,
                HttpEntity.EMPTY,
                new ParameterizedTypeReference<List<Comment>>() {}
        );

        final ResponseEntity<List<Comment>> exchangeWithVarArgs = restTemplate.exchange(
                BASE_URL + "/posts/{postId}/comments",
                HttpMethod.GET,
                HttpEntity.EMPTY,
                new ParameterizedTypeReference<List<Comment>>() {},
                urlVariables
        );

        final ResponseEntity<List<Comment>> exchangeWithVarArgsAndURI = restTemplate.exchange(
                url,
                HttpMethod.GET,
                HttpEntity.EMPTY,
                new ParameterizedTypeReference<List<Comment>>() {}
        );
        return exchange;
    }
}