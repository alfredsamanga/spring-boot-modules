# ResiliaTech Modules
This repository contains code for the articles on [resiliatech.com](https://resiliatech.com/).

This module is linked to the following article: 
[Consuming Restful Webservices in Spring Boot](https://resiliatech.com/consuming-rest-with-spring-boot)
