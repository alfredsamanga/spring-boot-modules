package controllers;

import play.libs.concurrent.Futures;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.*;
import service.RecordService;

import javax.inject.Inject;
import java.util.concurrent.CompletionStage;
import static java.util.concurrent.CompletableFuture.completedFuture;


public class HomeController extends Controller {
    private HttpExecutionContext ec;
    private Futures futures;
    private final RecordService recordService;

    @Inject
    public HomeController(HttpExecutionContext ec, Futures futures, RecordService recordService) {
        this.ec = ec;
        this.futures = futures;
        this.recordService = recordService;
    }

    public Result index() {
        return ok(views.html.index.render());
    }

    public CompletionStage<Result> indexPromise() {
        return completedFuture(ok(views.html.index.render()));
    }

    public CompletionStage<Result> getRecord(Http.Request request, Long recordId) {
        return recordService.getRecordById(recordId)
                .thenApplyAsync(recordOptional -> recordOptional
                        .map(record -> ok(record.toString()))
                        .orElse(notFound()), ec.current());
    }
}
