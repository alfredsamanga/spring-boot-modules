package service;

import ec.DatabaseExecutionContext;
import play.libs.concurrent.Futures;

import javax.inject.Inject;
import java.util.Optional;
import java.util.concurrent.CompletionStage;

import static java.util.concurrent.CompletableFuture.*;


public class RecordService {
    private final Futures futures;
    private final DatabaseExecutionContext executionContext;

    @Inject
    public RecordService(Futures futures, DatabaseExecutionContext executionContext) {
        this.executionContext = executionContext;
        this.futures = futures;
    }

    public CompletionStage<Optional<Record>> getRecordById(Long id) {
        return supplyAsync(() -> {
            //potentially blocking operation, or lengthy computation
            return Optional.of(new Record());
        }, executionContext);
    }
}
