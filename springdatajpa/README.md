# ResiliaTech Modules
This repository contains code for the articles on [resiliatech.com](https://resiliatech.com/).

This module is linked to the following article: 
[Relational Databases With Spring Boot](https://resiliatech.com/relational-db-spring-boot-and-spring-data-jpa)
