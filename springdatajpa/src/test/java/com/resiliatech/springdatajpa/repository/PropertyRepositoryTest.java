package com.resiliatech.springdatajpa.repository;

import com.resiliatech.springdatajpa.Util;
import com.resiliatech.springdatajpa.model.Property;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;

@ExtendWith(SpringExtension.class)
@DataJpaTest
public class PropertyRepositoryTest {
    @Autowired
    private TestEntityManager em;

    @Autowired
    private PropertyRepository propertyRepository;

    private Property property;

    @BeforeEach
    public void init() {
        property = Util.getProperty(1L);
        em.persist(property);
    }

    @AfterEach
    public void destroy() {
        if(property != null) {
            em.remove(property);
            property = null;
        }
    }

    @Test
    public void givenNewProperty_whenSave_thenShouldReturnSavedProperty() throws Exception {
        final Property savedProperty = propertyRepository.save(Util.getProperty(2L));
        assertAll(
                () -> assertThat(savedProperty).isNotNull(),
                () -> assertThat(savedProperty.getDescription()).isEqualTo("Test Property Description"),
                () -> assertThat(savedProperty.getId()).isEqualTo(2L)
        );
    }

    @Test
    public void givenPropertyId_whenFindById_thenShouldReturnProperty() throws Exception {
        final Optional<Property> fetchedPropertyOptional = propertyRepository.findById(1L);
        assertAll(
                () -> assertThat(fetchedPropertyOptional).isNotNull(),
                () -> assertThat(fetchedPropertyOptional.get().getId()).isEqualTo(1L),
                () -> assertThat(fetchedPropertyOptional.get().getDescription()).isEqualTo("Test Property Description")
        );
    }

    @Test
    public void givenExistingProperty_whenUpdate_thenShouldReturnUpdatedProperty() throws Exception {
        property.setDescription("Test Property Description");
        final Property updatedProperty = propertyRepository.save(property);
        assertAll(
                () -> assertThat(updatedProperty).isNotNull(),
                () -> assertThat(updatedProperty.getId()).isEqualTo(1L),
                () -> assertThat(updatedProperty.getDescription()).isEqualTo("Test Property Description")
        );
    }

    @Test
    public void givenExistingProperty_whenDelete_thenPropertyRecordMustNotBeFound() throws Exception {
        propertyRepository.delete(property);
        final Optional<Property> fetchedPropertyOptional = propertyRepository.findById(1L);
        assertAll(
                () -> assertThat(fetchedPropertyOptional).isNotNull(),
                () -> assertThat(fetchedPropertyOptional).isEmpty()
        );
    }
}
