package com.resiliatech.springdatajpa.repository;
import com.resiliatech.springdatajpa.Util;
import com.resiliatech.springdatajpa.model.Owner;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Optional;

import com.resiliatech.springdatajpa.model.Property;

import com.resiliatech.springdatajpa.model.Address;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;

@ExtendWith(SpringExtension.class)
@DataJpaTest
public class AddressRepositoryTest {
    @Autowired
    private TestEntityManager em;

    @Autowired
    private AddressRepository addressRepository;

    private Address address;

    @BeforeEach
    public void init() {
        address = Util.getAddress(1L);
        em.persist(address);
    }

    @AfterEach
    public void destroy() {
        if(address != null) {
            em.remove(address);
            address = null;
        }
    }

    @Test
    public void givenNewAddress_whenSave_thenShouldReturnSavedAddress() throws Exception {
        final Address savedAddress = addressRepository.save(Util.getAddress(2L));
        assertAll(
                () -> assertThat(savedAddress).isNotNull(),
                () -> assertThat(savedAddress.getCity()).isEqualTo("Harare"),
                () -> assertThat(savedAddress.getId()).isEqualTo(2L)
        );
    }

    @Test
    public void givenAddressId_whenFindById_thenShouldReturnAddress() throws Exception {
        final Optional<Address> fetchedAddressOptional = addressRepository.findById(1L);
        assertAll(
                () -> assertThat(fetchedAddressOptional).isNotNull(),
                () -> assertThat(fetchedAddressOptional.get().getId()).isEqualTo(1L),
                () -> assertThat(fetchedAddressOptional.get().getCity()).isEqualTo("Harare")
        );
    }

    @Test
    public void givenExistingAddress_whenUpdate_thenShouldReturnUpdatedAddress() throws Exception {
        address.setLine1("New Address");
        final Address updatedAddress = addressRepository.save(address);
        assertAll(
                () -> assertThat(updatedAddress).isNotNull(),
                () -> assertThat(updatedAddress.getId()).isEqualTo(1L),
                () -> assertThat(updatedAddress.getLine1()).isEqualTo("New Address")
        );
    }

    @Test
    public void givenExistingAddress_whenDelete_thenAddressRecordMustNotBeFound() throws Exception {
        addressRepository.delete(address);
        final Optional<Address> fetchedAddressOptional = addressRepository.findById(1L);
        assertAll(
                () -> assertThat(fetchedAddressOptional).isNotNull(),
                () -> assertThat(fetchedAddressOptional).isEmpty()
        );
    }
}
