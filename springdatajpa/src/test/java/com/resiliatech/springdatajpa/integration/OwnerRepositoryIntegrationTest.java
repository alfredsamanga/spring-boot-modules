package com.resiliatech.springdatajpa.integration;

import com.resiliatech.springdatajpa.Util;
import com.resiliatech.springdatajpa.model.Address;
import com.resiliatech.springdatajpa.model.Owner;
import com.resiliatech.springdatajpa.model.Property;
import com.resiliatech.springdatajpa.repository.AddressRepository;
import com.resiliatech.springdatajpa.repository.OwnerRepository;
import com.resiliatech.springdatajpa.repository.PropertyRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Commit;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
//@ActiveProfiles("mysql")
@ActiveProfiles("postgres")
public class OwnerRepositoryIntegrationTest {

    @Autowired
    private OwnerRepository ownerRepository;

    @Autowired
    private PropertyRepository propertyRepository;

    @Autowired
    private AddressRepository addressRepository;

    private Owner owner;

    @BeforeEach
    public void init() throws Exception {
        owner = Util.getOwner(1L);
        ownerRepository.save(owner);
    }

    @AfterEach
    public void destroy() throws Exception {
        ownerRepository.deleteById(1L);
        owner = null;
    }

    @Test
    public void givenNewOwner_whenSave_thenShouldReturnSavedOwner() throws Exception {
        final Owner savedOwner = ownerRepository.save(Util.getOwner(2L));
        assertAll(
                () -> assertThat(savedOwner).isNotNull(),
                () -> assertThat(savedOwner.getAddress()).isEqualTo("Somewhere"),
                () -> assertThat(savedOwner.getId()).isEqualTo(2L)
        );
    }

    @Test
    @Transactional
    public void givenNewOwnerAndProperties_whenSave_thenShouldReturnSavedOwnerAndProperties() throws Exception {
        final Owner owner = Util.getOwner(3L);
        final Owner savedOwner = ownerRepository.save(owner);
        addOwnerProperties(savedOwner);
        final Optional<Owner> fetchedOwnerOptional = ownerRepository.findById(3L);
        assertAll(
                () -> assertThat(fetchedOwnerOptional).isNotNull(),
                () -> assertThat(fetchedOwnerOptional).isPresent(),
                () -> assertThat(fetchedOwnerOptional.get().getAddress()).isEqualTo("Somewhere"),
                () -> assertThat(fetchedOwnerOptional.get().getProperties()).isNotNull(),
                () -> assertThat(fetchedOwnerOptional.get().getProperties()).isNotEmpty(),
                () -> assertThat(fetchedOwnerOptional.get().getProperties().size()).isEqualTo(5),
                () -> assertThat(fetchedOwnerOptional.get().getProperties().get(0).getAddress()).isNotNull(),
                () -> assertThat(fetchedOwnerOptional.get().getProperties().get(0).getAddress().getLine2()).isNotBlank(),
                () -> assertThat(fetchedOwnerOptional.get().getId()).isEqualTo(3L)
        );
    }

    @Test
    public void givenOwnerId_whenFindById_thenShouldReturnOwner() throws Exception {
        final Optional<Owner> fetchedOwnerOptional = ownerRepository.findById(1L);
        assertAll(
                () -> assertThat(fetchedOwnerOptional).isNotNull(),
                () -> assertThat(fetchedOwnerOptional).isPresent(),
                () -> assertThat(fetchedOwnerOptional.get().getId()).isEqualTo(1L),
                () -> assertThat(fetchedOwnerOptional.get().getAddress()).isEqualTo("Somewhere")
        );
    }

    @Test
    public void givenExistingOwner_whenUpdate_thenShouldReturnUpdatedOwner() throws Exception {
        owner.setAddress("Somewhere Updated");
        final Owner updatedOwner = ownerRepository.save(owner);
        assertAll(
                () -> assertThat(updatedOwner).isNotNull(),
                () -> assertThat(updatedOwner.getId()).isEqualTo(1L),
                () -> assertThat(updatedOwner.getAddress()).isEqualTo("Somewhere Updated")
        );
    }

    @Test
    public void givenExistingOwner_whenDelete_thenOwnerRecordMustNotBeFound() throws Exception {
        final Owner newOwner = ownerRepository.save(Util.getOwner(5L));
        final Optional<Owner> justCreatedOptional = ownerRepository.findById(5L);
        assertThat(justCreatedOptional).isPresent();
        assertThat(newOwner).isNotNull();
        ownerRepository.deleteById(5L);
        final Optional<Owner> fetchedOwnerOptional = ownerRepository.findById(5L);
        assertAll(
                () -> assertThat(fetchedOwnerOptional).isNotNull(),
                () -> assertThat(fetchedOwnerOptional).isEmpty()
        );
    }


    @Test
    public void givenFirstName_whenFindByFirstName_thenShouldReturnNonEmptyList() throws Exception {
        List<Owner> owners = ownerRepository.findAllByFirstName("John");
        assertAll(
                () -> assertThat(owners).isNotNull(),
                () -> assertThat(owners).isNotEmpty(),
                () -> assertTrue(owners.size() > 0)
        );
    }


    @Test
    public void givenLastName_whenFindByLastName_thenShouldReturnNonEmptyList() throws Exception {
        List<Owner> owners = ownerRepository.findAllByLastName("Doe");
        assertAll(
                () -> assertThat(owners).isNotNull(),
                () -> assertThat(owners).isNotEmpty(),
                () -> assertTrue(owners.size() > 0)
        );
    }


    @Test
    public void givenPartOfAddress_whenFindByAddressContaining_thenShouldReturnNonEmptyList() throws Exception {
        List<Owner> owners = ownerRepository.findAllByAddressContaining("Somewhere");
        assertAll(
                () -> assertThat(owners).isNotNull(),
                () -> assertThat(owners).isNotEmpty(),
                () -> assertTrue(owners.size() > 0)
        );
    }

    private void addOwnerProperties(Owner savedOwner) {
        final List<Property> properties = Util.getProperties();
        properties.forEach(property -> {
            property.setOwner(savedOwner);
            addressRepository.save(property.getAddress());
            propertyRepository.save(property);
        });
        savedOwner.setProperties(properties);
        ownerRepository.save(savedOwner);
    }
}
