package com.resiliatech.springdatajpa;

import com.resiliatech.springdatajpa.model.Address;
import com.resiliatech.springdatajpa.model.Owner;
import com.resiliatech.springdatajpa.model.Property;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.stream.Collectors;

public interface Util {

    static Address getAddress(long id) {
        final Address address = new Address();
        address.setName("Test Address");
        address.setLine1("Line 1");
        address.setLine2("Line 2");
        address.setCity("Harare");
        address.setCountry("Zimbabwe");
        address.setId(id);
        address.setDateCreated(OffsetDateTime.now());
        address.setLastUpdated(OffsetDateTime.now());
        return address;
    }

    static Property getProperty(long id) {
        final Property property = new Property();
        property.setName("Test Property");
        property.setDescription("Test Property Description");
        property.setOwner(getOwner(id));
        property.setAddress(getAddress(id));
        property.setId(id);
        property.setDateCreated(OffsetDateTime.now());
        property.setLastUpdated(OffsetDateTime.now());
        return property;
    }

    static Owner getOwner(long id) {
        final Owner owner = new Owner();
        owner.setFirstName("John");
        owner.setLastName("Doe");
        owner.setAddress("Somewhere");
        owner.setId(id);
        owner.setDateCreated(OffsetDateTime.now());
        owner.setLastUpdated(OffsetDateTime.now());
        return owner;
    }

    static List<Property> getProperties() {
        return List.of(1, 2, 3, 4, 5).stream().map(Util::getProperty).collect(Collectors.toList());
    }
}
