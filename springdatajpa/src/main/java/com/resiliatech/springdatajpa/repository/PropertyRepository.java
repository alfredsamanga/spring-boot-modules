package com.resiliatech.springdatajpa.repository;

import com.resiliatech.springdatajpa.model.Property;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PropertyRepository extends JpaRepository<Property, Long> {
}
