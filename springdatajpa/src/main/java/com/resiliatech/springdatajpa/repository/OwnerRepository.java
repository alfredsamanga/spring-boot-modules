package com.resiliatech.springdatajpa.repository;

import com.resiliatech.springdatajpa.model.Owner;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OwnerRepository extends JpaRepository<Owner, Long> {
    List<Owner> findAllByFirstName(String firstName);
    List<Owner> findAllByLastName(String lastName);
    List<Owner> findAllByAddressContaining(String part);
}
