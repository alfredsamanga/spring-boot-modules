package com.resiliatech.springdatajpa.model;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.OffsetDateTime;

@Data
@MappedSuperclass
public abstract class BaseEntity {
    @Id
    private Long id;

    @CreationTimestamp
    private OffsetDateTime dateCreated;

    @UpdateTimestamp
    private OffsetDateTime lastUpdated;

    @Version
    private Long version;
}
