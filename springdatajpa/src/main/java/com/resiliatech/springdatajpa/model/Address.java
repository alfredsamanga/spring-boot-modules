package com.resiliatech.springdatajpa.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@Entity
public class Address extends BaseEntity {

    private String name;
    private String line1;
    private String line2;
    private String city;
    private String country;

    @OneToOne(targetEntity = Property.class)
    private Property property;
}
