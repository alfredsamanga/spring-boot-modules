package com.resiliatech.springdatajpa.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@Entity
public class Property extends BaseEntity {
    private String name;
    private String description;

    @ManyToOne(targetEntity = Owner.class)
    private Owner owner;

    @OneToOne(mappedBy = "property")
    private Address address;

    @Override
    public String toString() {
        return "Property{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
