package com.resiliatech.springdatajpa.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.List;

@Data
@NoArgsConstructor
@Entity
public class Owner extends BaseEntity {
    private String firstName;
    private String lastName;
    private String address;

    @OneToMany(mappedBy = "owner")
    private List<Property> properties;

    @Override
    public String toString() {
        return "Owner{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}
