package com.resiliatech.userinput;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
public class PersonServiceTest {
    @Autowired
    private PersonService personService;

    private Person person;

    @BeforeEach
    public void init() {
        final Optional<Person> personOptional = personService.create(getPerson());
        personOptional.ifPresent(value -> person = value);
    }

    @AfterEach
    public void destroy() {
        if(person != null) {
            personService.deleteById(person.getId());
        }
    }

    @Test
    public void givenPerson_whenCreate_shouldReturnCreatedPerson() throws Exception {
        final Optional<Person> personOptional = personService.create(getPerson());
        assertTrue(personOptional.isPresent(), "Person should be present");
        final Person person = personOptional.get();
        assertAll(
                () -> assertThat(person.getFirstName()).isEqualTo(person.getFirstName())
        );
    }

    @Test
    public void givenPerson_whenUpdate_shouldReturnUpdatedPerson() throws Exception {
        person.setLastName(person.getLastName() + " Updated");
        final Optional<Person> personOptional = personService.update(PersonForm.getPersonForm(person));
        assertTrue(personOptional.isPresent(), "Person should be present");
        final Person person = personOptional.get();
        assertAll(
                () -> assertThat(person.getLastName()).isEqualTo("Doe Updated")
        );
    }

    @Test
    public void givenPersonId_whenGetById_shouldReturnPerson() throws Exception {
        final Optional<Person> personOptional = personService.getById(person.getId());
        assertTrue(personOptional.isPresent(), "Person should be present");
        final Person person = personOptional.get();
        assertAll(
                () -> assertThat(person.getLastName()).isEqualTo("Doe")
        );
    }

    @Test
    public void whenList_shouldReturnNonEmptyList() throws Exception {
        final List<Person> people = personService.list();
        assertAll(
                () -> assertThat(people).isNotEmpty()
        );
    }

    private Person getPerson() {
        return Person.builder()
                .firstName("John")
                .lastName("Doe")
                .roles(List.of("ADMIN", "CONTRIBUTOR"))
                .build();
    }
}
