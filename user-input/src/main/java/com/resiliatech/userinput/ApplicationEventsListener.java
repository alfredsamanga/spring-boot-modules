package com.resiliatech.userinput;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Slf4j
@RequiredArgsConstructor
@Component
public class ApplicationEventsListener {
    private final StorageService storageService;

    @EventListener
    public void onApplicationEvent(ContextClosedEvent event) throws IOException {
        final Path path = Paths.get(storageService.getBaseDir());
        Files.walk(path)
                .map(Path::toFile)
                .forEach(File::delete);
    }
}
