package com.resiliatech.userinput;

import lombok.RequiredArgsConstructor;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.nio.file.Path;
import java.time.format.DateTimeFormatter;

@Controller
@RequiredArgsConstructor
public class PersonController {
    private final StorageService storageService;
    private final PersonService personService;
    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("dd MMMM yyyy");

    @GetMapping
    public String viewAll(Model model) {
        model.addAttribute("people", personService.list());
        model.addAttribute("formatter", FORMATTER);
        return "people";
    }

    @GetMapping("/create")
    public String createPage(Model model) {
        model.addAttribute("personForm", new PersonForm());
        return "create";
    }

    @GetMapping("/images/{imageName}")
    public ResponseEntity<Resource> getImage(@PathVariable("imageName") String imageName) {
        if (!"avartar.jpg".equals(imageName)) {
            Resource file = storageService.loadAsResource(imageName);
            return ResponseEntity.ok(file);
        } else {
            return getDefaultAvartar();
        }
    }

    @PostMapping("/create")
    public String createPerson(@Valid PersonForm personForm,
                               BindingResult bindingResult,
                               RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            return "create";
        } else {
            final Person person = personForm.getPerson();
            if (personForm.getPicture() != null && !personForm.getPicture().isEmpty()) {

                final Path path = storageService.storeFile(personForm.getPicture());
                person.setImageName(path.getFileName().toString());
            }
            return personService.create(person).map(p -> "redirect:/person/" + p.getId())
                    .orElseGet(() -> {
                        redirectAttributes.addFlashAttribute("errors", "Failed to create person.");
                        return "redirect:/";
                    });
        }
    }

    @GetMapping("/update/{personId}")
    public String updatePage(@PathVariable("personId") Long personId,
                             Model model, RedirectAttributes redirectAttributes) {
        return personService.getById(personId).map(person -> {
            model.addAttribute("personForm", PersonForm.getPersonForm(person));
            return "update";
        }).orElseGet(() -> {
            redirectAttributes.addFlashAttribute("errors", "Person not found.");
            return "redirect:/";
        });
    }

    @PostMapping("/update")
    public String updatePerson(@Valid PersonForm personForm, BindingResult bindingResult,
                               RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            return "update";
        } else {
            return personService.update(personForm).map(person -> {
                return "redirect:/person/" + person.getId();
            }).orElseGet(() -> {
                redirectAttributes.addFlashAttribute("errors", "Failed to update person.");
                return "redirect:/";
            });
        }
    }

    @GetMapping("/person/{personId}")
    public String viewPerson(@PathVariable("personId") Long personId, Model model, RedirectAttributes redirectAttributes) {
        return personService.getById(personId).map(person -> {
            model.addAttribute("person", person);
            model.addAttribute("formatter", FORMATTER);
            return "person";
        }).orElseGet(() -> {
            redirectAttributes.addFlashAttribute("errors", "Person not found.");
            return "redirect:/";
        });
    }

    private ResponseEntity<Resource> getDefaultAvartar() {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-type", "image/jpeg");
        Resource resource = new ClassPathResource("static/img/avartar.jpg");
        return new ResponseEntity<>(resource, headers, HttpStatus.OK);
    }
}
