package com.resiliatech.userinput;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.UUID;

@Slf4j
@RequiredArgsConstructor
@Service
public class FileSystemStorageService implements StorageService {
    @Value("${files.path}")
    private String baseDirectory;

    @Override
    public String getBaseDir() {
        return baseDirectory;
    }

    @Override
    public Path storeFile(MultipartFile file) {
        return store(file, Paths.get(getBaseDir()));
    }

    @Override
    public void deleteFile(String fileName) {
        try {
            Files.delete(Paths.get(getBaseDir()).resolve(fileName));
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("Failed to delete file", e);
        }
    }

    @Override
    public Resource loadAsResource(String fileName) {
        final Path path = Paths.get(getBaseDir()).resolve(fileName);
        try {
            Resource resource = new UrlResource(path.toUri());
            if (resource.exists() || resource.isReadable()) {
                return resource;
            }
            else {
                throw new RuntimeException("Could not read file: " + path);
            }
        }
        catch (MalformedURLException e) {
            throw new RuntimeException("Could not read file: " + path, e);
        }
    }

    private Path store(MultipartFile file, Path location) {
        String filename = StringUtils.cleanPath(file.getOriginalFilename());
        filename = generateFileName(filename);
        Path path = null;
        try {
            if (file.isEmpty()) {
                throw new RuntimeException("Failed to store empty file " + filename);
            }
            if (filename.contains("..")) {
                throw new RuntimeException(
                        "Cannot store file with relative path outside current directory "
                                + filename);
            }
            path = location.resolve(filename);
            try (InputStream inputStream = file.getInputStream()) {
                Files.copy(inputStream, path, StandardCopyOption.REPLACE_EXISTING);
            }
        }
        catch (IOException e) {
            throw new RuntimeException("Failed to store file " + filename, e);
        }
        return path;
    }

    static String generateFileName(String filename) {
        final String fileExtension = getFileExtension(filename);
        return String.format("%s%s", UUID.randomUUID().toString(), fileExtension);
    }

    static String getFileExtension(String fileName) {
        return fileName.substring(fileName.lastIndexOf("."));
    }
}
