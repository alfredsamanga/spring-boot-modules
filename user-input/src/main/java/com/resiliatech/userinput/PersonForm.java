package com.resiliatech.userinput;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.Arrays;

@Data
public class PersonForm {
    private Long id;
    @NotBlank(message = "First name is required")
    private String firstName;

    @NotBlank(message = "Last name is required")
    private String lastName;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate dateOfBirth;

    @Email
    private String email;
    private String address;
    private String address2;

    private MultipartFile picture;

    @Size(min = 1, max = 5, message = "Select between 1 and 5 roles")
    private String[] roles;

    public Person getPerson() {
        return Person.builder()
                .id(id)
                .firstName(firstName)
                .lastName(lastName)
                .dateOfBirth(dateOfBirth)
                .email(email)
                .address(address)
                .address2(address2)
                .roles(Arrays.asList(roles))
                .imageName(picture.getOriginalFilename())
                .build();
    }

    public static PersonForm getPersonForm(Person person) {
        final PersonForm personForm = new PersonForm();
        personForm.setId(person.getId());
        personForm.setFirstName(person.getFirstName());
        personForm.setLastName(person.getLastName());
        personForm.setEmail(person.getEmail());
        personForm.setAddress(person.getAddress());
        personForm.setAddress2(person.getAddress2());
        personForm.setDateOfBirth(person.getDateOfBirth());
        personForm.setRoles(person.getRoles().toArray(new String[0]));
        return personForm;
    }
}
