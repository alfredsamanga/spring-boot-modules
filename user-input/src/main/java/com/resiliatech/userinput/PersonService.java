package com.resiliatech.userinput;


import java.util.List;
import java.util.Optional;

public interface PersonService {
    Optional<Person> create(Person person);
    Optional<Person> update(PersonForm personForm);
    List<Person> list();
    Optional<Person> getById(Long id);
    Optional<Person> deleteById(Long id);
}
