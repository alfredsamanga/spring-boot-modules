package com.resiliatech.userinput;

import lombok.Builder;
import lombok.Data;
import org.springframework.util.StringUtils;

import java.time.LocalDate;
import java.util.List;

@Data
@Builder
public class Person {
    private Long id;
    private String firstName;
    private String lastName;
    private LocalDate dateOfBirth;
    private String email;
    private String address;
    private String address2;
    private String imageName;
    private List<String> roles;

    public String getImageName() {
        return StringUtils.hasLength(imageName)? imageName: "avartar.jpg";
    }

    public String getFormattedRoles() {
        return String.join(", ", roles);
    }
}
