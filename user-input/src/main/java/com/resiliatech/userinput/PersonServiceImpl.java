package com.resiliatech.userinput;

import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class PersonServiceImpl implements PersonService {
    private Map<Long, Person> personMap = new HashMap<>();

    @Override
    public Optional<Person> create(Person person) {
        final long personId = personMap.size() + 1L;
        person.setId(personId);
        personMap.put(personId, person);
        return Optional.ofNullable(person);
    }

    @Override
    public Optional<Person> update(PersonForm personForm) {
        final Person result = personMap.computeIfPresent(personForm.getId(), (personId, p) -> {
            p.setFirstName(personForm.getFirstName());
            p.setLastName(personForm.getLastName());
            p.setAddress(personForm.getAddress());
            p.setAddress2(personForm.getAddress2());
            p.setDateOfBirth(personForm.getDateOfBirth());
            p.setEmail(personForm.getEmail());
            p.setRoles(Arrays.asList(personForm.getRoles()));
            return p;
        });
        return Optional.ofNullable(result);
    }

    @Override
    public List<Person> list() {
        return new ArrayList<>(personMap.values());
    }

    @Override
    public Optional<Person> getById(Long id) {
        return Optional.ofNullable(personMap.get(id));
    }

    @Override
    public Optional<Person> deleteById(Long id) {
        final Person removed = personMap.remove(id);
        return Optional.ofNullable(removed);
    }
}
