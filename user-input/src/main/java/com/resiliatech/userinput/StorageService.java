package com.resiliatech.userinput;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;

public interface StorageService {
    String getBaseDir();
    Path storeFile(MultipartFile file);
    Resource loadAsResource(String fileName);
    void deleteFile(String fileName);
}
