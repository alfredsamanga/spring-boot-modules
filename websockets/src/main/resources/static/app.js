let stompClient = null;
let contextClasses = ['danger', 'info', 'warning', 'success'];

window.onbeforeunload = disconnect();

function prependMessage(message) {
    $("#current").html("<div class='alert alert-" + getRandomContextClass() + "'>" + message + "</td></tr>");
    $("#notifications").prepend("<div class='alert alert-info'>" + message + "</td></tr>");
}

function getRandomContextClass() {
    let randomIndex = Math.floor(Math.random() * 4)
    return contextClasses[randomIndex];
}

function onConnected() {
    $("#notifications").html("");
    sendRequest();
}

function connect() {
    let socket = new SockJS('/websocket');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        onConnected();
        console.log('Connected: ' + frame);
        stompClient.subscribe('/topic/notifications', function (message) {
            let notification = message.body;
            prependMessage(notification);
        });
    });
}

function disconnect() {
    if (stompClient !== null) {
        stompClient.disconnect();
    }
    prependMessage("Disconnected");
}

function sendRequest() {
    stompClient.send("/app/notifications", {},
        JSON.stringify({
            'message': 'notifications'
        })
    );
}

$(function () {
    connect();
});