package com.resiliatech.websockets;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

import java.util.Random;

@Slf4j
@Controller
@RequiredArgsConstructor
public class NotificationController {
    private final SimpMessagingTemplate webSocket;
    private static Random random = new Random();


//    @MessageMapping("/notifications")
//    @SendTo("/topic/notifications")
//    public RequestMessage forwardMessage(RequestMessage requestMessage) {
//        return requestMessage;
//    }

    @MessageMapping("/notifications")
    public void handleRequest(RequestMessage requestMessage) {
        sendRandomNotifications();
    }

    private void sendRandomNotifications() {
        while(true) {
            int seconds = 1 + random.nextInt(10);
            long sleepTime = seconds * 1000L;
            try {
                Thread.sleep(sleepTime);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            webSocket.convertAndSend("/topic/notifications", Quotes.getRandomQuote());
        }
    }
}
