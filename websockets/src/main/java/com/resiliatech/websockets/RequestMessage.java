package com.resiliatech.websockets;

import lombok.Data;

@Data
public class RequestMessage {
    private String message;
}
