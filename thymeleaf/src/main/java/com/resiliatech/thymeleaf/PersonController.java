package com.resiliatech.thymeleaf;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class PersonController {
    private List<Person> people;

    @GetMapping
    public String indexPage(Model model) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        model.addAttribute("people", getPeople());
        model.addAttribute("df", formatter);
        return "people";
    }

    @GetMapping("/{id}")
    public String detailsPage(Model model, @PathVariable("id") int id) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        model.addAttribute("user", getPerson(id));
        model.addAttribute("df", formatter);
        return "details";
    }

    private List<Person> getPeople() {
        if (people == null) {
            people = IntStream.rangeClosed(1, 10).mapToObj(value -> {
                Person person = new Person();
                person.setId(value);
                person.setFirstName("John " + value);
                person.setLastName("Doe " + value);
                person.setDateOfBirth(LocalDate.of(2009, value, 21));
                person.setAddress("Borrowdale, Harare");
                person.setEmail("email@example.com");
                person.setPhone("12345" + value);
                person.setShowDetails(ThreadLocalRandom.current().nextBoolean());
                return person;
            }).collect(Collectors.toList());
        }
        return people;
    }

    private Person getPerson(int id) {
        List<Person> people = getPeople();
        return people.stream().filter(person -> person.getId() == id).findFirst().orElse(null);
    }
}
