# ResiliaTech Modules
This repository contains code for the articles on [resiliatech.com](https://resiliatech.com/).

This module is linked to the following article: [Thymeleaf Templates With Spring Boot](https://resiliatech.com/thymeleaf-spring-boot/)
